﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [First_Name] NVARCHAR(50) NOT NULL, 
    [Last_Name] NVARCHAR(50) NOT NULL, 
    [Address] NVARCHAR(50) NOT NULL, 
    [Phone#] NVARCHAR(50) NOT NULL, 
    [Phone#_2] NVARCHAR(50) NULL, 
    [Building_ID] INT NOT NULL, 
    CONSTRAINT [FK_Employee_Building] FOREIGN KEY (Building_ID) REFERENCES Building(Id)
)
