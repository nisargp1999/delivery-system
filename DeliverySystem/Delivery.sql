﻿CREATE TABLE [dbo].[Delivery]
(
	[Package_Id] INT NOT NULL PRIMARY KEY, 
    [Address] NVARCHAR(50) NOT NULL, 
    [Ship_date] NVARCHAR(50) NOT NULL, 
    [Origin_id] INT NOT NULL, 
    CONSTRAINT [FK_Delivery_Package] FOREIGN KEY (Package_id) REFERENCES Package(id), 
    CONSTRAINT [FK_Delivery_Building] FOREIGN KEY (Origin_id) REFERENCES Building(id)
)
