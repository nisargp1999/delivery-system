﻿CREATE TABLE [dbo].[Package]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Length] INT NOT NULL, 
    [Width] INT NOT NULL, 
    [Height] INT NULL, 
    [Weight] FLOAT NULL, 
    [Destination] NVARCHAR(50) NOT NULL, 
    [Shipper_id] INT NOT NULL, 
    CONSTRAINT [FK_Package_ToTable] FOREIGN KEY (Shipper_id) REFERENCES Customer(id)
)
