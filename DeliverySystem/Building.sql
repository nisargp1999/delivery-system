﻿CREATE TABLE [dbo].[Building]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Address] NVARCHAR(50) NOT NULL, 
    [Open_time] NVARCHAR(50) NULL DEFAULT '8am', 
    [Close_time] NCHAR(10) NULL DEFAULT '8pm'
)
