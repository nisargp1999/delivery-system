﻿CREATE TABLE [dbo].[Completes]
(
	[Package_Id] INT NOT NULL , 
    [Building_Id] INT NOT NULL, 
    [Delivery_date] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_Completes_Building] FOREIGN KEY (Building_Id) REFERENCES Building(Id), 
    CONSTRAINT [FK_Completes_Package] FOREIGN KEY (Package_Id) REFERENCES Package(Id), 
    PRIMARY KEY (Package_Id, Building_id)
)
