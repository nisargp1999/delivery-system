﻿CREATE TABLE [dbo].[Customer]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [First_name] NVARCHAR(50) NOT NULL, 
    [Last_name] NVARCHAR(50) NOT NULL, 
    [Phone#] NVARCHAR(50) NOT NULL, 
    [Address] NVARCHAR(50) NOT NULL
)
