﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/* used information provided by Professor Eric Charnesky to build database using Entity Framework
         * https://github.com/EricCharnesky/CIS297-Winter2019/tree/master/CollegeRegistration 
         
    * https://stackoverflow.com/questions/30578790/open-another-form-from-main-form-and-then-close-the-main-form-on-c-sharp-project 
 https://stackoverflow.com/questions/19726884/convert-iqueryable-to-bindinglist 
https://www.youtube.com/watch?v=hg3H_pAzoPI&t=1160s 
             ************** To see the ER diagram generated, double click on "DeliveryService_Model.edmx, just
             above "Form1.cs"*/

namespace DeliveryService
{
    public partial class ShipmentForm : Form
    {
        DeliverySystemEntities2 shipment;
        bool IsDelivery { get; set; }
        public ShipmentForm()
        {
            InitializeComponent();
            shipment = new DeliverySystemEntities2();
            IsDelivery = true;
            // Loads databases
            shipment.Buildings.Load();
            shipment.Customers.Load();
            shipment.Completes.Load();
            shipment.Deliveries.Load();

            // set the datasource of the Package  and Building List Boxes
            BuildingInfo_List.DataSource = shipment.Buildings.Local.ToBindingList();
            BuildingInfo_List.DisplayMember = nameof(Building.Building_Information);

            PackageInfo_List.DataSource = shipment.Packages.Local.ToBindingList();
            PackageInfo_List.DisplayMember = nameof(Package.Package_Information);

            Update_Buildings();
            Update_Packages();
          
            Hide_items();
            Clear_Textboxes();

         
        }

        private void SwitchTo_Package_Click(object sender, EventArgs e)
        {
            this.Hide();
            var switch_Package = new PackageForm();
            switch_Package.Show();
        }

        private void SwitchTo_Customer_Click(object sender, EventArgs e)
        {
            this.Hide();
            var switch_Customer = new CustomerForm();
            switch_Customer.Show();
        }

        private void SwitchTo_Employee_Click(object sender, EventArgs e)
        {
            this.Hide();
            var switch_Employee = new EmployeeForm();
            switch_Employee.Show();
        }
        private void Button_color_Reset()
        {
            
        }
        /************DELIVERY OPTIONS******************/
        private void AddDelivery_Button_Click(object sender, EventArgs e)
        {
            Hide_items();
            Reset_Button_Color();
            AddDelivery_Button.BackColor = Color.DarkGray;
            ShipDate_Label.Visible = true;
            ShipDate_Textbox.Visible = true;

            Address_Label.Visible = true;
            Address_Textbox.Visible = true;
            PackageInfo_List.Visible = true;
            BuildingInfo_List.Visible = true;

            AddDelivery_Submit.Visible = true;
        }

        private void ViewDelivery_Data_Button_Click(object sender, EventArgs e)
        {
            Hide_items();
            Reset_Button_Color();
            ViewDelivery_Data_Button.BackColor = Color.DarkGray;
            Delivery_DataGridView.Visible = true;
            SearchID_Label.Visible = true;
            SearchID_Textbox.Visible = true;
            Search_button.Visible = true;
            Sort_By_Building.Visible = true;
            Update_DeliveryTable();
            IsDelivery = true;
        }
        /************COMPLETE OPTIONS******************/
        private void AddComplete_Button_Click(object sender, EventArgs e)
        {
            Hide_items();
            Reset_Button_Color();
            AddComplete_Button.BackColor = Color.DarkGray;
            DeliveryDate_Label.Visible = true;
            DeliveryDate_TextBox.Visible = true;

            BuildingInfo_List.Visible = true;
            PackageInfo_List.Visible = true;

            AddComplete_Submit.Visible = true; 

        }

        private void ViewCompleted_Data_Button_Click(object sender, EventArgs e)
        {
            Hide_items();
            Reset_Button_Color();
            ViewCompleted_Data_Button.BackColor = Color.DarkGray;
          
            Complete_DataGridView.Visible = true;
            SearchID_Label.Visible = true;
            SearchID_Textbox.Visible = true;
            Search_button.Visible = true;
            Sort_By_Building.Visible = true;
            Update_CompleteTable();
            IsDelivery = false;
        }
        /************SUBMIT BUTTONS******************/
        private void AddComplete_Submit_Click(object sender, EventArgs e)
        {

            if(DeliveryDate_TextBox.Text == String.Empty || PackageInfo_List.SelectedItem == null || BuildingInfo_List.SelectedItem == null)
            {
                MessageBox.Show("Please enter a Delivery Date  as well as select a Customer ID and Building ID");
            }
            else
            {
                Complete new_CompletedDelivery = new Complete
                {
                    Delivery_date= DeliveryDate_TextBox.Text,
                    Building = BuildingInfo_List.SelectedItem as Building,
                    Package = PackageInfo_List.SelectedItem as Package
                };
                shipment.Completes.Add(new_CompletedDelivery);
                Reset_Button_Color();
                shipment.SaveChanges();
                Clear_Textboxes();
            }
           
        }

        private void AddDelivery_Submit_Click(object sender, EventArgs e)
        {
            if (ShipDate_Textbox.Text == String.Empty ||Address_Textbox.Text == String.Empty || PackageInfo_List.SelectedItem == null || BuildingInfo_List.SelectedItem == null )
            {
                MessageBox.Show("Please enter a Ship Date and Address as well as select a Customer ID and Building ID");
            }
            else
            {
                Delivery New_Delivery = new Delivery
                {
                    Package = PackageInfo_List.SelectedItem as Package,
                    Building = BuildingInfo_List.SelectedItem as Building,
                    Ship_date = ShipDate_Textbox.Text,
                    Address = Address_Textbox.Text
                };
                shipment.Deliveries.Add(New_Delivery);
                Reset_Button_Color();
                shipment.SaveChanges();
                Clear_Textboxes();
            }
            
        }
        /************AUXILLARY FUNCTIONS******************/
        private void Update_Buildings()
        {
            foreach (var element in shipment.Buildings) {
                BuildingInfo_List.Text += $"{element.Id} {element.Address}";
            }
        }
        private void Update_Packages()
        {
            foreach( var element in shipment.Packages)
            {
                PackageInfo_List.Text += $"{element.Id} {element.Customer.First_name} {element.Customer.Last_name}";
            }
        }
        private void Hide_items()
        {
            ShipDate_Label.Visible = false;
            Address_Label.Visible = false;

            ShipDate_Textbox.Visible = false;
            Address_Textbox.Visible = false;

            AddComplete_Submit.Visible = false;
            AddDelivery_Submit.Visible = false;

            DeliveryDate_Label.Visible = false;
            DeliveryDate_TextBox.Visible = false;

            PackageInfo_List.Visible = false;
           
            BuildingInfo_List.Visible = false;

            Complete_DataGridView.Visible = false;
            Delivery_DataGridView.Visible = false;

            SearchID_Label.Visible = false;
            SearchID_Textbox.Visible = false;
            Search_button.Visible = false;

            ResetDataGridView_Button.Visible = false;
            Sort_By_Building.Visible = false;
      


        }
         private void Clear_Textboxes()
         {
            ShipDate_Textbox.Text = String.Empty;
            Address_Textbox.Text = String.Empty;
            SearchID_Textbox.Text = String.Empty;
           

            PackageInfo_List.SelectedItem = null;
            BuildingInfo_List.SelectedItem = null;
         }
        private void Update_DeliveryTable()
        {
            Delivery_DataGridView.AutoGenerateColumns = false;
            Delivery_DataGridView.RowHeadersVisible = false;
            Delivery_DataGridView.DataSource = shipment.Deliveries.ToList<Delivery>();
        }
        private void Update_CompleteTable()
        {
            Complete_DataGridView.AutoGenerateColumns = false;
            Complete_DataGridView.RowHeadersVisible = false;
            Complete_DataGridView.DataSource = shipment.Completes.ToList<Complete>();
        }




        /************FILTER/SORT FUNCTIONS******************/
        private void Sort_By_Building_Click(object sender, EventArgs e)
        {
            if (IsDelivery)
            {
                var SortQuery = from Delivery delivery in shipment.Deliveries
                                orderby delivery.Origin_id
                                select delivery;
                Delivery_DataGridView.DataSource = SortQuery.ToList();
                Delivery_DataGridView.AutoGenerateColumns = false;
                Delivery_DataGridView.RowHeadersVisible = false;

            }
            else
            {
                var SortQuery = from Complete completed_delivery in shipment.Completes
                               orderby completed_delivery.Building_Id
                                select completed_delivery;
                Complete_DataGridView.DataSource = SortQuery.ToList();
                Complete_DataGridView.AutoGenerateColumns = false;
                Complete_DataGridView.RowHeadersVisible = false;
            }
            ResetDataGridView_Button.Visible = true;
        }
        private void Search_button_Click(object sender, EventArgs e)
        {
            if(IsDelivery)
            {
                int temp = Convert.ToInt32(SearchID_Textbox.Text);
                var SearchQuery = from Delivery delivery in shipment.Deliveries
                                  where delivery.Package_Id == temp
                                  select delivery;
               
                Delivery_DataGridView.DataSource = SearchQuery.ToList();
                Delivery_DataGridView.AutoGenerateColumns = false;
                Delivery_DataGridView.RowHeadersVisible = false;
                ResetDataGridView_Button.Visible = true;
               
               
            }
            else
            {
                int temp = Convert.ToInt32(SearchID_Textbox.Text);
                var SearchQuery = from Complete completed_delivery in shipment.Completes
                                  where completed_delivery.Package_Id == temp
                                  select completed_delivery;
              
                   Complete_DataGridView.DataSource = SearchQuery.ToList();
                
                ResetDataGridView_Button.Visible = true;
              
            }
            
        }

        private void ResetDataGridView_Button_Click(object sender, EventArgs e)
        {
            Clear_Textboxes();
            if (IsDelivery)
            {
                Update_DeliveryTable();
                
            }
            else
            {
                Update_CompleteTable();
            }
         
         
        }

        private void Reset_Button_Color()
        {
            AddDelivery_Button.BackColor = Color.LightGray;
            AddDelivery_Button.ForeColor = Color.Black;
            ViewDelivery_Data_Button.BackColor = Color.LightGray;
            ViewDelivery_Data_Button.ForeColor = Color.Black;




            AddComplete_Button.BackColor = Color.LightGray;
            AddComplete_Button.ForeColor = Color.Black;
            ViewCompleted_Data_Button.BackColor = Color.LightGray;
            ViewCompleted_Data_Button.ForeColor = Color.Black;

        }
        /*********************EXIT PROGRAM**********************/
        private void Exit_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ShipmentForm_Load(object sender, EventArgs e)
        {

        }
    }
}
