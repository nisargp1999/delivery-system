﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeliveryService
{


     public partial class CustomerForm : Form
     {
          private DeliverySystemEntities2 DeliverySystemCustomer;
          public CustomerForm()
          {
               InitializeComponent();
               DeliverySystemCustomer = new DeliverySystemEntities2();
               DeliverySystemCustomer.Customers.Load();
               DeliverySystemCustomer.Packages.Load();

               dataGrid.DataSource = DeliverySystemCustomer.Customers.ToList<Customer>();
               dataGrid.AutoGenerateColumns = false;

               reset();
               
               
               
          }

          private void reset()
          {
               addCustomerButton.BackColor = Color.LightGray;
               addCustomerButton.ForeColor = Color.Black;
               updateCustomerButton.BackColor = Color.LightGray;
               updateCustomerButton.ForeColor = Color.Black;
               removeCustomerButton.BackColor = Color.LightGray;
               removeCustomerButton.ForeColor = Color.Black;
               viewCustomerButton.BackColor = Color.LightGray;
               viewCustomerButton.ForeColor = Color.Black;

               submitButton.Visible = true;

               idText.Visible = true;
               firstNameText.Visible = true;
               lastNameText.Visible = true;
               phoneNumberText.Visible = true;
               addressText.Visible = true;

               customerIdLabel.Visible = true;
               customerFirstNameLabel.Visible = true;
               customerLastNameLabel.Visible = true;
               customerPhoneNumberLabel.Visible = true;
               customerAddressLabel.Visible = true;

               idText.ReadOnly = false;
               firstNameText.ReadOnly = false;
               lastNameText.ReadOnly = false;
               phoneNumberText.ReadOnly = false;
               addressText.ReadOnly = false;

               idText.Text = String.Empty;
               firstNameText.Text = String.Empty;
               lastNameText.Text = String.Empty;
               phoneNumberText.Text = String.Empty;
               addressText.Text = String.Empty;

               dataGrid.Visible = false;
          }

          private void add()
          {
               if (firstNameText.Text == String.Empty ||
                    lastNameText.Text == String.Empty ||
                    phoneNumberText.Text == String.Empty ||
                    addressText.Text == String.Empty)
               {
                    if (firstNameText.Text == String.Empty)
                    {
                         MessageBox.Show("First name field is empty");
                    }
                    if (lastNameText.Text == String.Empty)
                    {
                         MessageBox.Show("Last name field is empty");
                    }
                    if (phoneNumberText.Text == String.Empty)
                    {
                         MessageBox.Show("Phone number field is empty");
                    }
                    if (addressText.Text == String.Empty)
                    {
                         MessageBox.Show("Address field is empty");
                    }
               }
               else
               {
                    Customer tempCustomer = new Customer()
                    {
                         First_name = firstNameText.Text,
                         Last_name = lastNameText.Text,
                         Phone_ = phoneNumberText.Text,
                         Address = addressText.Text
                    };
                    DeliverySystemCustomer.Customers.Add(tempCustomer);
                    DeliverySystemCustomer.SaveChanges();
                    dataGrid.DataSource = DeliverySystemCustomer.Customers.ToList<Customer>();
               }
          }

          private void addCustomerButton_Click(object sender, EventArgs e)
          {
               reset();
               addCustomerButton.BackColor = Color.DarkGray;
               //addCustomerButton.ForeColor = Color.White;

               idText.ReadOnly = true;
          }

          private void update()
          {
               if (idText.Text == String.Empty)
               {
                    MessageBox.Show("Please type in the customer id.");
               }
               else if (firstNameText.Text == String.Empty &&
                    lastNameText.Text == String.Empty &&
                    phoneNumberText.Text == String.Empty &&
                    addressText.Text == String.Empty)
               {
                    MessageBox.Show("All the data fields are empty.\nThere is nothing to update");
               }
               else
               {
                    int temp = Convert.ToInt32(idText.Text);
                    var updateQuery = from Customer tempCustomer in DeliverySystemCustomer.Customers
                                      where tempCustomer.Id == temp
                                      select tempCustomer;

                    foreach (var result in updateQuery)
                    {
                         if (firstNameText.Text != String.Empty)
                              result.First_name = firstNameText.Text;
                         if (lastNameText.Text != String.Empty)
                              result.Last_name = lastNameText.Text;
                         if (phoneNumberText.Text != String.Empty)
                         {
                              if (phoneNumberText.Text.Length != 10)
                              {
                                   MessageBox.Show("Invalid Phone #");
                              }
                              else
                              {
                                   result.Phone_ = phoneNumberText.Text;
                              }
                         }
                         if (addressText.Text != String.Empty)
                              result.Address = addressText.Text;
                    }
                    DeliverySystemCustomer.SaveChanges();
                    dataGrid.DataSource = DeliverySystemCustomer.Customers.ToList<Customer>();

               }
               
          }

          private void updateCustomerButton_Click(object sender, EventArgs e)
          {
               reset();
               updateCustomerButton.BackColor = Color.DarkGray;
               //updateCustomerButton.ForeColor = Color.White;

          }

          private void remove()
          {
               if (idText.Text == String.Empty)
               {
                    MessageBox.Show("Please type in the customer id.");
               }
               else
               {
                    int temp = Convert.ToInt32(idText.Text);
                    var removeQuery = from Customer tempCustomer in DeliverySystemCustomer.Customers
                                      where tempCustomer.Id == temp
                                      select tempCustomer;

                    var foreignKey = from Package tempPackage in DeliverySystemCustomer.Packages
                                     where tempPackage.Shipper_id == temp
                                     select tempPackage;

                    if (removeQuery.Count() < 1)       //if id does not exist
                    {
                         MessageBox.Show("This id does not exist.");
                    }
                    else if (foreignKey.Count() < 1)        //If it's not in package table
                    {
                         foreach (var result in removeQuery)
                         {
                              DeliverySystemCustomer.Customers.Remove(result);
                         }

                         DeliverySystemCustomer.SaveChanges();
                    }
                    else
                    {
                         MessageBox.Show("This id is a foreign key for package table");
                    }
               }
          }

          private void removeCustomerButton_Click(object sender, EventArgs e)
          {
               reset();
               removeCustomerButton.BackColor = Color.DarkGray;
               //removeCustomerButton.ForeColor = Color.White;

               firstNameText.ReadOnly = true;
               lastNameText.ReadOnly = true;
               phoneNumberText.ReadOnly = true;
               addressText.ReadOnly = true;

          }

          private void submitButton_Click(object sender, EventArgs e)
          {

               if (addCustomerButton.BackColor == Color.DarkGray)
               {
                    add();
                    reset();
               }
               else if (updateCustomerButton.BackColor == Color.DarkGray)
               {
                    update();
                    reset();
               }
               else if (removeCustomerButton.BackColor == Color.DarkGray)
               {
                    remove();
                    reset();
               }
               //else if (viewCustomerButton.BackColor == Color.DarkGray)
               //{
               //     view();
               //     reset();
               //}
               else
               {
                    MessageBox.Show("None of the buttons are clicked.");
               }



          }

          //private void view()
          //{

          //}

          private void viewCustomerButton_Click(object sender, EventArgs e)
          {
               reset();

               viewCustomerButton.BackColor = Color.DarkGray;
               //viewCustomerButton.ForeColor = Color.White;

               idText.Visible = false;
               firstNameText.Visible = false;
               lastNameText.Visible = false;
               phoneNumberText.Visible = false;
               addressText.Visible = false;

               customerIdLabel.Visible = false;
               customerFirstNameLabel.Visible = false;
               customerLastNameLabel.Visible = false;
               customerPhoneNumberLabel.Visible = false;
               customerAddressLabel.Visible = false;

               submitButton.Visible = false;
               dataGrid.DataSource = DeliverySystemCustomer.Customers.ToList<Customer>();
               dataGrid.Visible = true;
          }

          private void menuEmployee_Click(object sender, EventArgs e)
          {
            this.Hide();
            var switch_Employee = new EmployeeForm();
            switch_Employee.Show();
            //to be updated
          }

          private void menuPackage_Click(object sender, EventArgs e)
          {
               PackageForm newForm = new PackageForm();
               newForm.Show();
               this.Hide();
          }

          private void button1_Click(object sender, EventArgs e)
          {
               this.Close();
          }

          private void CustomerForm_Load(object sender, EventArgs e)
          {
               //ignore but do not delete
          }

          private void shipmentButton_Click(object sender, EventArgs e)
          {
            this.Hide();
            var switch_Shipment = new ShipmentForm();
            switch_Shipment.Show();
          }
     }
     
}
