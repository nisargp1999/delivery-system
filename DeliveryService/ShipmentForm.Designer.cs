﻿namespace DeliveryService
{
    partial class ShipmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Menu_GroupBox = new System.Windows.Forms.GroupBox();
            this.SwitchTo_Customer = new System.Windows.Forms.Button();
            this.SwitchTo_Employee = new System.Windows.Forms.Button();
            this.SwitchTo_Package = new System.Windows.Forms.Button();
            this.AddComplete_Button = new System.Windows.Forms.Button();
            this.ViewCompleted_Data_Button = new System.Windows.Forms.Button();
            this.Address_Textbox = new System.Windows.Forms.TextBox();
            this.Address_Label = new System.Windows.Forms.Label();
            this.ShipDate_Label = new System.Windows.Forms.Label();
            this.ShipDate_Textbox = new System.Windows.Forms.TextBox();
            this.AddDelivery_Submit = new System.Windows.Forms.Button();
            this.AddComplete_Submit = new System.Windows.Forms.Button();
            this.PackageInfo_List = new System.Windows.Forms.ListBox();
            this.BuildingInfo_List = new System.Windows.Forms.ListBox();
            this.DeliveryDate_Label = new System.Windows.Forms.Label();
            this.DeliveryDate_TextBox = new System.Windows.Forms.TextBox();
            this.Delivery_DataGridView = new System.Windows.Forms.DataGridView();
            this.packageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.originID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Complete_DataGridView = new System.Windows.Forms.DataGridView();
            this.PackageID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuildingID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SearchID_Textbox = new System.Windows.Forms.TextBox();
            this.SearchID_Label = new System.Windows.Forms.Label();
            this.Search_button = new System.Windows.Forms.Button();
            this.ResetDataGridView_Button = new System.Windows.Forms.Button();
            this.Sort_By_Building = new System.Windows.Forms.Button();
            this.Exit_Button = new System.Windows.Forms.Button();
            this.ViewDelivery_Data_Button = new System.Windows.Forms.Button();
            this.AddDelivery_Button = new System.Windows.Forms.Button();
            this.Menu_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Delivery_DataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Complete_DataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Menu_GroupBox
            // 
            this.Menu_GroupBox.Controls.Add(this.SwitchTo_Customer);
            this.Menu_GroupBox.Controls.Add(this.Exit_Button);
            this.Menu_GroupBox.Controls.Add(this.SwitchTo_Employee);
            this.Menu_GroupBox.Controls.Add(this.SwitchTo_Package);
            this.Menu_GroupBox.Location = new System.Drawing.Point(12, 24);
            this.Menu_GroupBox.Name = "Menu_GroupBox";
            this.Menu_GroupBox.Size = new System.Drawing.Size(97, 257);
            this.Menu_GroupBox.TabIndex = 0;
            this.Menu_GroupBox.TabStop = false;
            this.Menu_GroupBox.Text = "Menu";
            // 
            // SwitchTo_Customer
            // 
            this.SwitchTo_Customer.Location = new System.Drawing.Point(6, 72);
            this.SwitchTo_Customer.Name = "SwitchTo_Customer";
            this.SwitchTo_Customer.Size = new System.Drawing.Size(77, 35);
            this.SwitchTo_Customer.TabIndex = 2;
            this.SwitchTo_Customer.Text = "Customer";
            this.SwitchTo_Customer.UseVisualStyleBackColor = true;
            this.SwitchTo_Customer.Click += new System.EventHandler(this.SwitchTo_Customer_Click);
            // 
            // SwitchTo_Employee
            // 
            this.SwitchTo_Employee.Location = new System.Drawing.Point(6, 128);
            this.SwitchTo_Employee.Name = "SwitchTo_Employee";
            this.SwitchTo_Employee.Size = new System.Drawing.Size(77, 35);
            this.SwitchTo_Employee.TabIndex = 3;
            this.SwitchTo_Employee.Text = "Employee";
            this.SwitchTo_Employee.UseVisualStyleBackColor = true;
            this.SwitchTo_Employee.Click += new System.EventHandler(this.SwitchTo_Employee_Click);
            // 
            // SwitchTo_Package
            // 
            this.SwitchTo_Package.Location = new System.Drawing.Point(6, 19);
            this.SwitchTo_Package.Name = "SwitchTo_Package";
            this.SwitchTo_Package.Size = new System.Drawing.Size(77, 35);
            this.SwitchTo_Package.TabIndex = 1;
            this.SwitchTo_Package.Text = "Package";
            this.SwitchTo_Package.UseVisualStyleBackColor = true;
            this.SwitchTo_Package.Click += new System.EventHandler(this.SwitchTo_Package_Click);
            // 
            // AddComplete_Button
            // 
            this.AddComplete_Button.Location = new System.Drawing.Point(445, 24);
            this.AddComplete_Button.Name = "AddComplete_Button";
            this.AddComplete_Button.Size = new System.Drawing.Size(116, 35);
            this.AddComplete_Button.TabIndex = 2;
            this.AddComplete_Button.Text = "Add Completed Shipments";
            this.AddComplete_Button.UseVisualStyleBackColor = true;
            this.AddComplete_Button.Click += new System.EventHandler(this.AddComplete_Button_Click);
            // 
            // ViewCompleted_Data_Button
            // 
            this.ViewCompleted_Data_Button.Location = new System.Drawing.Point(598, 24);
            this.ViewCompleted_Data_Button.Name = "ViewCompleted_Data_Button";
            this.ViewCompleted_Data_Button.Size = new System.Drawing.Size(116, 35);
            this.ViewCompleted_Data_Button.TabIndex = 4;
            this.ViewCompleted_Data_Button.Text = "View Completed Shipment Data";
            this.ViewCompleted_Data_Button.UseVisualStyleBackColor = true;
            this.ViewCompleted_Data_Button.Click += new System.EventHandler(this.ViewCompleted_Data_Button_Click);
            // 
            // Address_Textbox
            // 
            this.Address_Textbox.Location = new System.Drawing.Point(246, 126);
            this.Address_Textbox.Name = "Address_Textbox";
            this.Address_Textbox.Size = new System.Drawing.Size(139, 20);
            this.Address_Textbox.TabIndex = 7;
            // 
            // Address_Label
            // 
            this.Address_Label.AutoSize = true;
            this.Address_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Address_Label.Location = new System.Drawing.Point(164, 124);
            this.Address_Label.Name = "Address_Label";
            this.Address_Label.Size = new System.Drawing.Size(72, 20);
            this.Address_Label.TabIndex = 8;
            this.Address_Label.Text = "Address:";
            // 
            // ShipDate_Label
            // 
            this.ShipDate_Label.AutoSize = true;
            this.ShipDate_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShipDate_Label.Location = new System.Drawing.Point(152, 78);
            this.ShipDate_Label.Name = "ShipDate_Label";
            this.ShipDate_Label.Size = new System.Drawing.Size(84, 20);
            this.ShipDate_Label.TabIndex = 9;
            this.ShipDate_Label.Text = "Ship Date:";
            // 
            // ShipDate_Textbox
            // 
            this.ShipDate_Textbox.Location = new System.Drawing.Point(246, 80);
            this.ShipDate_Textbox.Name = "ShipDate_Textbox";
            this.ShipDate_Textbox.Size = new System.Drawing.Size(100, 20);
            this.ShipDate_Textbox.TabIndex = 10;
            // 
            // AddDelivery_Submit
            // 
            this.AddDelivery_Submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddDelivery_Submit.Location = new System.Drawing.Point(337, 205);
            this.AddDelivery_Submit.Name = "AddDelivery_Submit";
            this.AddDelivery_Submit.Size = new System.Drawing.Size(82, 38);
            this.AddDelivery_Submit.TabIndex = 13;
            this.AddDelivery_Submit.Text = "Submit";
            this.AddDelivery_Submit.UseVisualStyleBackColor = true;
            this.AddDelivery_Submit.Click += new System.EventHandler(this.AddDelivery_Submit_Click);
            // 
            // AddComplete_Submit
            // 
            this.AddComplete_Submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddComplete_Submit.Location = new System.Drawing.Point(337, 205);
            this.AddComplete_Submit.Name = "AddComplete_Submit";
            this.AddComplete_Submit.Size = new System.Drawing.Size(82, 38);
            this.AddComplete_Submit.TabIndex = 14;
            this.AddComplete_Submit.Text = "Submit";
            this.AddComplete_Submit.UseVisualStyleBackColor = true;
            this.AddComplete_Submit.Click += new System.EventHandler(this.AddComplete_Submit_Click);
            // 
            // PackageInfo_List
            // 
            this.PackageInfo_List.FormattingEnabled = true;
            this.PackageInfo_List.Location = new System.Drawing.Point(425, 78);
            this.PackageInfo_List.Name = "PackageInfo_List";
            this.PackageInfo_List.Size = new System.Drawing.Size(352, 121);
            this.PackageInfo_List.TabIndex = 15;
            // 
            // BuildingInfo_List
            // 
            this.BuildingInfo_List.FormattingEnabled = true;
            this.BuildingInfo_List.Location = new System.Drawing.Point(425, 205);
            this.BuildingInfo_List.Name = "BuildingInfo_List";
            this.BuildingInfo_List.Size = new System.Drawing.Size(352, 121);
            this.BuildingInfo_List.TabIndex = 16;
            // 
            // DeliveryDate_Label
            // 
            this.DeliveryDate_Label.AutoSize = true;
            this.DeliveryDate_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeliveryDate_Label.Location = new System.Drawing.Point(129, 167);
            this.DeliveryDate_Label.Name = "DeliveryDate_Label";
            this.DeliveryDate_Label.Size = new System.Drawing.Size(107, 20);
            this.DeliveryDate_Label.TabIndex = 17;
            this.DeliveryDate_Label.Text = "Delivery Date:";
            // 
            // DeliveryDate_TextBox
            // 
            this.DeliveryDate_TextBox.Location = new System.Drawing.Point(246, 169);
            this.DeliveryDate_TextBox.Name = "DeliveryDate_TextBox";
            this.DeliveryDate_TextBox.Size = new System.Drawing.Size(100, 20);
            this.DeliveryDate_TextBox.TabIndex = 18;
            // 
            // Delivery_DataGridView
            // 
            this.Delivery_DataGridView.AllowUserToDeleteRows = false;
            this.Delivery_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Delivery_DataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.packageID,
            this.shipDate,
            this.originID,
            this.ShipAddress});
            this.Delivery_DataGridView.Location = new System.Drawing.Point(156, 78);
            this.Delivery_DataGridView.Name = "Delivery_DataGridView";
            this.Delivery_DataGridView.ReadOnly = true;
            this.Delivery_DataGridView.Size = new System.Drawing.Size(558, 188);
            this.Delivery_DataGridView.TabIndex = 19;
            // 
            // packageID
            // 
            this.packageID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.packageID.DataPropertyName = "Package_Id";
            this.packageID.HeaderText = "Package ID";
            this.packageID.Name = "packageID";
            this.packageID.ReadOnly = true;
            this.packageID.Width = 89;
            // 
            // shipDate
            // 
            this.shipDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.shipDate.DataPropertyName = "Ship_date";
            this.shipDate.HeaderText = "Ship Date";
            this.shipDate.Name = "shipDate";
            this.shipDate.ReadOnly = true;
            this.shipDate.Width = 79;
            // 
            // originID
            // 
            this.originID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.originID.DataPropertyName = "origin_id";
            this.originID.HeaderText = "Origin ID";
            this.originID.Name = "originID";
            this.originID.ReadOnly = true;
            this.originID.Width = 73;
            // 
            // ShipAddress
            // 
            this.ShipAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ShipAddress.DataPropertyName = "Address";
            this.ShipAddress.HeaderText = "Address ";
            this.ShipAddress.Name = "ShipAddress";
            this.ShipAddress.ReadOnly = true;
            // 
            // Complete_DataGridView
            // 
            this.Complete_DataGridView.AllowUserToDeleteRows = false;
            this.Complete_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Complete_DataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PackageID2,
            this.BuildingID2,
            this.DeliveryDate});
            this.Complete_DataGridView.Location = new System.Drawing.Point(156, 78);
            this.Complete_DataGridView.Name = "Complete_DataGridView";
            this.Complete_DataGridView.ReadOnly = true;
            this.Complete_DataGridView.Size = new System.Drawing.Size(558, 188);
            this.Complete_DataGridView.TabIndex = 20;
            // 
            // PackageID2
            // 
            this.PackageID2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PackageID2.DataPropertyName = "Package_Id";
            this.PackageID2.HeaderText = "Package ID";
            this.PackageID2.Name = "PackageID2";
            this.PackageID2.ReadOnly = true;
            // 
            // BuildingID2
            // 
            this.BuildingID2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BuildingID2.DataPropertyName = "Building_Id";
            this.BuildingID2.HeaderText = "Building ID";
            this.BuildingID2.Name = "BuildingID2";
            this.BuildingID2.ReadOnly = true;
            // 
            // DeliveryDate
            // 
            this.DeliveryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeliveryDate.DataPropertyName = "Delivery_date";
            this.DeliveryDate.HeaderText = "Delivery Date ";
            this.DeliveryDate.Name = "DeliveryDate";
            this.DeliveryDate.ReadOnly = true;
            // 
            // SearchID_Textbox
            // 
            this.SearchID_Textbox.Location = new System.Drawing.Point(322, 275);
            this.SearchID_Textbox.Name = "SearchID_Textbox";
            this.SearchID_Textbox.Size = new System.Drawing.Size(100, 20);
            this.SearchID_Textbox.TabIndex = 21;
            // 
            // SearchID_Label
            // 
            this.SearchID_Label.AutoSize = true;
            this.SearchID_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchID_Label.Location = new System.Drawing.Point(149, 273);
            this.SearchID_Label.Name = "SearchID_Label";
            this.SearchID_Label.Size = new System.Drawing.Size(167, 20);
            this.SearchID_Label.TabIndex = 22;
            this.SearchID_Label.Text = "Search by Package ID";
            // 
            // Search_button
            // 
            this.Search_button.Location = new System.Drawing.Point(306, 301);
            this.Search_button.Name = "Search_button";
            this.Search_button.Size = new System.Drawing.Size(116, 35);
            this.Search_button.TabIndex = 23;
            this.Search_button.Text = "Search";
            this.Search_button.UseVisualStyleBackColor = true;
            this.Search_button.Click += new System.EventHandler(this.Search_button_Click);
            // 
            // ResetDataGridView_Button
            // 
            this.ResetDataGridView_Button.Location = new System.Drawing.Point(3, 349);
            this.ResetDataGridView_Button.Name = "ResetDataGridView_Button";
            this.ResetDataGridView_Button.Size = new System.Drawing.Size(116, 35);
            this.ResetDataGridView_Button.TabIndex = 24;
            this.ResetDataGridView_Button.Text = "Reset";
            this.ResetDataGridView_Button.UseVisualStyleBackColor = true;
            this.ResetDataGridView_Button.Click += new System.EventHandler(this.ResetDataGridView_Button_Click);
            // 
            // Sort_By_Building
            // 
            this.Sort_By_Building.Location = new System.Drawing.Point(3, 301);
            this.Sort_By_Building.Name = "Sort_By_Building";
            this.Sort_By_Building.Size = new System.Drawing.Size(116, 35);
            this.Sort_By_Building.TabIndex = 25;
            this.Sort_By_Building.Text = "Sort By Building";
            this.Sort_By_Building.UseVisualStyleBackColor = true;
            this.Sort_By_Building.Click += new System.EventHandler(this.Sort_By_Building_Click);
            // 
            // Exit_Button
            // 
            this.Exit_Button.BackColor = System.Drawing.Color.Red;
            this.Exit_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit_Button.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Exit_Button.Location = new System.Drawing.Point(6, 179);
            this.Exit_Button.Name = "Exit_Button";
            this.Exit_Button.Size = new System.Drawing.Size(77, 35);
            this.Exit_Button.TabIndex = 26;
            this.Exit_Button.Text = "EXIT";
            this.Exit_Button.UseVisualStyleBackColor = false;
            this.Exit_Button.Click += new System.EventHandler(this.Exit_Button_Click);
            // 
            // ViewDelivery_Data_Button
            // 
            this.ViewDelivery_Data_Button.Location = new System.Drawing.Point(306, 24);
            this.ViewDelivery_Data_Button.Name = "ViewDelivery_Data_Button";
            this.ViewDelivery_Data_Button.Size = new System.Drawing.Size(116, 35);
            this.ViewDelivery_Data_Button.TabIndex = 1;
            this.ViewDelivery_Data_Button.Text = "View Delivery Data";
            this.ViewDelivery_Data_Button.UseVisualStyleBackColor = true;
            this.ViewDelivery_Data_Button.Click += new System.EventHandler(this.ViewDelivery_Data_Button_Click);
            // 
            // AddDelivery_Button
            // 
            this.AddDelivery_Button.Location = new System.Drawing.Point(156, 24);
            this.AddDelivery_Button.Name = "AddDelivery_Button";
            this.AddDelivery_Button.Size = new System.Drawing.Size(116, 35);
            this.AddDelivery_Button.TabIndex = 3;
            this.AddDelivery_Button.Text = "Add Delivery";
            this.AddDelivery_Button.UseVisualStyleBackColor = true;
            this.AddDelivery_Button.Click += new System.EventHandler(this.AddDelivery_Button_Click);
            // 
            // ShipmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ViewCompleted_Data_Button);
            this.Controls.Add(this.AddComplete_Button);
            this.Controls.Add(this.ViewDelivery_Data_Button);
            this.Controls.Add(this.AddDelivery_Button);
            this.Controls.Add(this.Delivery_DataGridView);
            this.Controls.Add(this.Sort_By_Building);
            this.Controls.Add(this.Complete_DataGridView);
            this.Controls.Add(this.ResetDataGridView_Button);
            this.Controls.Add(this.Search_button);
            this.Controls.Add(this.SearchID_Label);
            this.Controls.Add(this.SearchID_Textbox);
            this.Controls.Add(this.DeliveryDate_TextBox);
            this.Controls.Add(this.DeliveryDate_Label);
            this.Controls.Add(this.BuildingInfo_List);
            this.Controls.Add(this.PackageInfo_List);
            this.Controls.Add(this.AddDelivery_Submit);
            this.Controls.Add(this.AddComplete_Submit);
            this.Controls.Add(this.ShipDate_Textbox);
            this.Controls.Add(this.ShipDate_Label);
            this.Controls.Add(this.Address_Label);
            this.Controls.Add(this.Address_Textbox);
            this.Controls.Add(this.Menu_GroupBox);
            this.Name = "ShipmentForm";
            this.Text = "ShipmentForm";
            this.Load += new System.EventHandler(this.ShipmentForm_Load);
            this.Menu_GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Delivery_DataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Complete_DataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox Menu_GroupBox;
        private System.Windows.Forms.Button SwitchTo_Customer;
        private System.Windows.Forms.Button SwitchTo_Employee;
        private System.Windows.Forms.Button SwitchTo_Package;
        private System.Windows.Forms.Button AddComplete_Button;
        private System.Windows.Forms.Button ViewCompleted_Data_Button;
        private System.Windows.Forms.TextBox Address_Textbox;
        private System.Windows.Forms.Label Address_Label;
        private System.Windows.Forms.Label ShipDate_Label;
        private System.Windows.Forms.TextBox ShipDate_Textbox;
        private System.Windows.Forms.Button AddDelivery_Submit;
        private System.Windows.Forms.Button AddComplete_Submit;
        private System.Windows.Forms.ListBox PackageInfo_List;
        private System.Windows.Forms.ListBox BuildingInfo_List;
        private System.Windows.Forms.Label DeliveryDate_Label;
        private System.Windows.Forms.TextBox DeliveryDate_TextBox;
        private System.Windows.Forms.DataGridView Delivery_DataGridView;
        private System.Windows.Forms.DataGridView Complete_DataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackageID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuildingID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryDate;
        private System.Windows.Forms.TextBox SearchID_Textbox;
        private System.Windows.Forms.Label SearchID_Label;
        private System.Windows.Forms.Button Search_button;
        private System.Windows.Forms.Button ResetDataGridView_Button;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn originID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipAddress;
        private System.Windows.Forms.Button Sort_By_Building;
        private System.Windows.Forms.Button Exit_Button;
        private System.Windows.Forms.Button ViewDelivery_Data_Button;
        private System.Windows.Forms.Button AddDelivery_Button;
    }
}