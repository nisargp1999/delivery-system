﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DeliveryService
{
    public partial class EmployeeForm : Form
    {
        DeliverySystemEntities2 emp;
        bool add = false, del = false, up = false;
        
        public EmployeeForm()
        {
            emp = new DeliverySystemEntities2();
            InitializeComponent();

            emp.Employees.Load();
            emp.Buildings.Load();

            buildLBox.DataSource= emp.Buildings.Local.ToBindingList();
            buildLBox.DisplayMember = nameof(Building.Id);
            
            updateTable();
            makeHidden();   
        }

        private void makeHidden()
        {
            // Make Labels Hidden
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;

            // Make TextBox Hidden
            ssnBox.Visible = false;
            fnameBox.Visible = false;
            lnameBox.Visible = false;
            addrBox.Visible = false;
            phone1Box.Visible = false;
            phone2Box.Visible = false;


            // Make action Button Hidden
            submitBtn.Visible = false;

            // make listbox invisible
            buildLBox.Visible = false;

            //hide gridview
            empBox.Visible = false;
            bBox.Visible = false;

            //clear option selections
            add = false;
            del = false;
            up = false;

            //reset button colors
            addBtn.BackColor = Color.LightGray;
            removeBtn.BackColor = Color.LightGray;
            updateBtn.BackColor = Color.LightGray;
            viewBtn.BackColor = Color.LightGray;
            vBuildBtn.BackColor = Color.LightGray;
        }

        //OPTIONS
        private void addBtn_Click(object sender, EventArgs e)
        {
            makeHidden();
            add = true;
            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            
            ssnBox.Visible = true;
            fnameBox.Visible = true;
            lnameBox.Visible = true;
            addrBox.Visible = true;
            phone1Box.Visible = true;
            phone2Box.Visible = true;

            buildLBox.Visible = true;

            submitBtn.Visible = true;

            addBtn.BackColor = Color.DarkGray;
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            makeHidden();
            del = true;
            label1.Visible = true;
            ssnBox.Visible = true;
            submitBtn.Visible = true;

            removeBtn.BackColor = Color.DarkGray;
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            makeHidden();
            up = true;
            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;

            ssnBox.Visible = true;
            fnameBox.Visible = true;
            lnameBox.Visible = true;
            addrBox.Visible = true;
            phone1Box.Visible = true;
            phone2Box.Visible = true;

            buildLBox.Visible = true;

            submitBtn.Visible = true;

            updateBtn.BackColor = Color.DarkGray;
        }

        private void viewBtn_Click(object sender, EventArgs e)
        {
            makeHidden();
            empBox.Visible = true;

            viewBtn.BackColor = Color.DarkGray;
        }

        private void vBuildBtn_Click(object sender, EventArgs e)
        {
            makeHidden();
            bBox.Visible = true;
            vBuildBtn.BackColor = Color.DarkGray;
        }




        //MENU CLICKS
        private void customerBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            
            var switch_customer = new CustomerForm();
            switch_customer.Show();
            

        }

        private void packageBtn_Click(object sender, EventArgs e)
        {
            var switch_package = new PackageForm();
            switch_package.Show();
            this.Hide();
        }

        private void shipmentBtn_Click(object sender, EventArgs e)
        {
            var switch_shipment = new ShipmentForm();
            switch_shipment.Show();
            this.Hide();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //ADD DELETE UPDATE
        private void submitBtn_Click(object sender, EventArgs e)
        {
            if (add == true)
            {
                int temp = Convert.ToInt32(ssnBox.Text);
                Building myB = new Building();
                myB = buildLBox.SelectedItem as Building;
                int temp2 = myB.Id;

                Employee empl = new Employee
                {
                    Id = temp,
                    First_Name = fnameBox.Text,
                    Last_Name = lnameBox.Text,
                    Address = addrBox.Text,
                    Phone_ = phone1Box.Text,
                    Phone__2 = phone2Box.Text,
                    Building_ID = temp2
                };
                emp.Employees.Add(empl);
                emp.SaveChanges();
                updateTable();
                clearTextBox();
            }
            else if (del == true)
            {
                int temp = Convert.ToInt32(ssnBox.Text);
                var Removequery = from Employee empl in emp.Employees
                                  where empl.Id == temp
                                  select empl;
                foreach (var element in Removequery)
                {
                    emp.Employees.Remove(element);
                }
                emp.SaveChanges();
                updateTable();
                clearTextBox();
            }
            else if (up == true)
            {
                int social = Convert.ToInt32(ssnBox.Text);
                var Updatequery = from Employee empl in emp.Employees
                                  where empl.Id == social
                                  select empl;

                foreach (var element in Updatequery)
                {
                    if (fnameBox.Text != String.Empty)
                    {
                        element.First_Name = fnameBox.Text;

                    }
                    if (lnameBox.Text != String.Empty)
                    {
                        element.Last_Name = lnameBox.Text;
                    }
                    if (addrBox.Text != String.Empty)
                    {
                        element.Address = addrBox.Text;
                    }
                    if (phone1Box.Text != String.Empty)
                    {
                        element.Phone_ = phone1Box.Text;
                    }
                    if (phone2Box.Text != String.Empty)
                    {
                        element.Phone__2 = phone2Box.Text;
                    }
                    if (buildLBox.SelectedItem != null)
                    {
                        Building myB = new Building();
                        myB = buildLBox.SelectedItem as Building;
                        int bID = myB.Id;
                        element.Building_ID = bID;
                    }
                }
          
                emp.SaveChanges();
                updateTable();
                clearTextBox();
            }

        }


        //EXTRA UTILITY FUNCTIONS
        private void updateTable()
        {
            empBox.AutoGenerateColumns = false;
            empBox.RowHeadersVisible = false;
            empBox.DataSource = emp.Employees.ToList<Employee>();

            bBox.AutoGenerateColumns = false;
            bBox.RowHeadersVisible = false;
            bBox.DataSource = emp.Buildings.ToList<Building>();
        }

        private void clearTextBox()
        {
            ssnBox.Text = String.Empty;
            fnameBox.Text = String.Empty;
            lnameBox.Text = String.Empty;
            addrBox.Text = String.Empty;
            phone1Box.Text = String.Empty;
            phone2Box.Text = String.Empty;
            buildLBox.SelectedItem = null;
        }

        private void employeeOptions_Enter(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void ssnBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void fnameBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void lnameBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void addrBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void phone1Box_TextChanged(object sender, EventArgs e)
        {

        }

        private void phone2Box_TextChanged(object sender, EventArgs e)
        {

        }

        private void buildLBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bBox_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void empLBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

    }
}
