﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeliveryService
{
     public partial class PackageForm : Form
     {
          private DeliverySystemEntities2 DeliverySystemPackage;
          public PackageForm()
          {
               InitializeComponent();
               reset();
               DeliverySystemPackage = new DeliverySystemEntities2();
               DeliverySystemPackage.Packages.Load();
               DeliverySystemPackage.Customers.Load();
               customerListBox.DataSource = DeliverySystemPackage.Customers.Local.ToBindingList();
               customerListBox.DisplayMember = nameof(Customer.customerDisplay);

               packageDataGrid.DataSource = DeliverySystemPackage.Packages.ToList<Package>();
               packageDataGrid.AutoGenerateColumns = false;
          }

          private void reset()
          {
               addPackageButton.BackColor = Color.LightGray;
               addPackageButton.ForeColor = Color.Black;
               updatePackageButton.BackColor = Color.LightGray;
               updatePackageButton.ForeColor = Color.Black;
               removePackageButton.BackColor = Color.LightGray;
               removePackageButton.ForeColor = Color.Black;
               viewPackageButton.BackColor = Color.LightGray;
               viewPackageButton.ForeColor = Color.Black;

               submitButton.Visible = true;

               idText.Visible = true;
               lengthText.Visible = true;
               widthText.Visible = true;
               heightText.Visible = true;
               weightText.Visible = true;
               destinationText.Visible = true;

               packageIdLabel.Visible = true;
               shipperIdLabel.Visible = true;
               packageLengthLabel.Visible = true;
               packageWidthLabel.Visible = true;
               packageHeightLabel.Visible = true;
               packageWeightLabel.Visible = true;
               packageDestinationLabel.Visible = true;
               updateCheckBox.Visible = false;

               idText.ReadOnly = false;
               lengthText.ReadOnly = false;
               widthText.ReadOnly = false;
               heightText.ReadOnly = false;
               weightText.ReadOnly = false;
               destinationText.ReadOnly = false;

               idText.Text = String.Empty;
               lengthText.Text = String.Empty;
               widthText.Text = String.Empty;
               heightText.Text = String.Empty;
               weightText.Text = String.Empty;
               destinationText.Text = String.Empty;

               updateCheckBox.Checked = false;
               shipperIdLabel.Visible = false;
               packageDataGrid.Visible = false;
               
          }

          private void PackageForm_Load(object sender, EventArgs e)
          {
               //ignore but do not delete
          }


          private void add()
          {
               if (lengthText.Text == String.Empty ||
                   widthText.Text == String.Empty ||
                   heightText.Text == String.Empty ||
                   weightText.Text == String.Empty ||
                   destinationText.Text == String.Empty)
               {
                    if (lengthText.Text == String.Empty)
                    {
                         MessageBox.Show("Length field is empty");
                    }
                    if (widthText.Text == String.Empty)
                    {
                         MessageBox.Show("Width field is empty");
                    }
                    if (heightText.Text == String.Empty)
                    {
                         MessageBox.Show("Height field is empty");
                    }
                    if (weightText.Text == String.Empty)
                    {
                         MessageBox.Show("Weight field is empty");
                    }
                    if (destinationText.Text == String.Empty)
                    {
                         MessageBox.Show("Destination address field is empty");
                    }
               }
               else
               {
                    var tempCustomer = customerListBox.SelectedItem as Customer;

                    Package newPackage = new Package()
                    {
                         Shipper_id = tempCustomer.Id,
                         Length = Convert.ToInt32(lengthText.Text),
                         Width = Convert.ToInt32(widthText.Text),
                         Height = Convert.ToInt32(heightText.Text),
                         Weight = Convert.ToInt32(weightText.Text),
                         Destination = destinationText.Text
                    };
                    DeliverySystemPackage.Packages.Add(newPackage);
                    DeliverySystemPackage.SaveChanges();
               }
          }

          private void addPackageButton_Click(object sender, EventArgs e)
          {
               reset();
               addPackageButton.BackColor = Color.DarkGray;
               //addPackageButton.ForeColor = Color.White;
               shipperIdLabel.Visible = true;
               customerListBox.Visible = true;

               idText.ReadOnly = true;
          }


          private void update()
          {
               if (idText.Text == String.Empty)
               {
                    MessageBox.Show("Please type in the package id.");
               }
               else if (!updateCheckBox.Checked &&
                        lengthText.Text == String.Empty &&
                        widthText.Text == String.Empty &&
                        heightText.Text == String.Empty &&
                        weightText.Text == String.Empty &&
                        destinationText.Text == String.Empty)
               {
                    MessageBox.Show("All the data fields are empty.\nThere is nothing to update");
               }
               else
               {
                    int temp = Convert.ToInt32(idText.Text);
                    var updateQuery = from Package tempPackage in DeliverySystemPackage.Packages
                                      where tempPackage.Id == temp
                                      select tempPackage;

                    foreach(var result in updateQuery)
                    {
                         if (lengthText.Text != String.Empty)
                              result.Length = Convert.ToInt32(lengthText.Text);
                         if (widthText.Text != String.Empty)
                              result.Width = Convert.ToInt32(widthText.Text);
                         if (heightText.Text != String.Empty)
                              result.Height = Convert.ToInt32(heightText.Text);
                         if (weightText.Text != String.Empty)
                              result.Weight = Convert.ToInt32(weightText.Text);
                         if (destinationText.Text != String.Empty)
                              result.Destination = destinationText.Text;
                         if (updateCheckBox.Checked)
                         {
                              var tempId = customerListBox.SelectedItem as Customer;
                              result.Shipper_id = tempId.Id;
                         }
                    }
                    DeliverySystemPackage.SaveChanges();
               }
          }

          private void updatePackageButton_Click(object sender, EventArgs e)
          {
               reset();
               updatePackageButton.BackColor = Color.DarkGray;
               //updatePackageButton.ForeColor = Color.White;
               customerListBox.Visible = true;
               updateCheckBox.Visible = true;
          }


          private void remove()
          {
               if (idText.Text == String.Empty)
               {
                    MessageBox.Show("Please type in the customer id.");
               }
               else
               {
                    int temp = Convert.ToInt32(idText.Text);
                    var removeQuery = from Package tempPackage in DeliverySystemPackage.Packages
                                       where tempPackage.Id == temp
                                       select tempPackage;

                    if (removeQuery.Count() < 1)
                    {
                         MessageBox.Show("This id does not exist.");
                    }
                    else
                    {
                         foreach(var result in removeQuery)
                         {
                              DeliverySystemPackage.Packages.Remove(result);
                         }
                         DeliverySystemPackage.SaveChanges();
                    }
               }
          }

          private void removePackageButton_Click(object sender, EventArgs e)
          {
               reset();
               removePackageButton.BackColor = Color.DarkGray;
               //removePackageButton.ForeColor = Color.White;

               lengthText.ReadOnly = true;
               widthText.ReadOnly = true;
               heightText.ReadOnly = true;
               weightText.ReadOnly = true;
               destinationText.ReadOnly = true;
               customerListBox.Visible = false;

               
          }

          private void viewPackageButton_Click(object sender, EventArgs e)
          {
               reset();
               packageDataGrid.Visible = true;
               viewPackageButton.BackColor = Color.DarkGray;
               //viewPackageButton.ForeColor = Color.White;

               packageDataGrid.DataSource = DeliverySystemPackage.Packages.ToList<Package>();

          }

          private void submitButton_Click(object sender, EventArgs e)
          {
               if (addPackageButton.BackColor == Color.DarkGray)
               {
                    add();
                    reset();
               }
               else if (updatePackageButton.BackColor == Color.DarkGray)
               {
                    update();
                    reset();
               }
               else if (removePackageButton.BackColor == Color.DarkGray)
               {
                    remove();
                    reset();
               }
               else
               {
                    MessageBox.Show("None of the buttons are clicked");
               }
          }

          private void menuCustomer_Click(object sender, EventArgs e)
          {
               CustomerForm newForm = new CustomerForm();
               newForm.Show();
               this.Hide();
          }

          private void menuEmployee_Click(object sender, EventArgs e)
          {
            this.Hide();
            var switch_Employee = new EmployeeForm();
            switch_Employee.Show();
               //To be updated
          }

          private void button1_Click(object sender, EventArgs e)
          {
               this.Close();
          }

          private void shipmentButton_Click(object sender, EventArgs e)
          {
            this.Hide();
            var switch_Shipment = new ShipmentForm();
            switch_Shipment.Show();
          }
     }

     public partial class Customer
     {
          public string customerDisplay => $"{First_name} {Last_name}  Phone#: {Phone_}  Address: {Address}";
     }
}
