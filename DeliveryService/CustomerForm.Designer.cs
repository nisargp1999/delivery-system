﻿namespace DeliveryService
{
     partial class CustomerForm
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.addCustomerButton = new System.Windows.Forms.Button();
               this.updateCustomerButton = new System.Windows.Forms.Button();
               this.removeCustomerButton = new System.Windows.Forms.Button();
               this.customerFirstNameLabel = new System.Windows.Forms.Label();
               this.customerLastNameLabel = new System.Windows.Forms.Label();
               this.firstNameText = new System.Windows.Forms.TextBox();
               this.lastNameText = new System.Windows.Forms.TextBox();
               this.customerPhoneNumberLabel = new System.Windows.Forms.Label();
               this.phoneNumberText = new System.Windows.Forms.TextBox();
               this.customerAddressLabel = new System.Windows.Forms.Label();
               this.addressText = new System.Windows.Forms.TextBox();
               this.submitButton = new System.Windows.Forms.Button();
               this.customerIdLabel = new System.Windows.Forms.Label();
               this.idText = new System.Windows.Forms.TextBox();
               this.dataGrid = new System.Windows.Forms.DataGridView();
               this.customer_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.customer_first_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.customer_last_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.customer_phone_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.customer_address = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.viewCustomerButton = new System.Windows.Forms.Button();
               this.menuPackage = new System.Windows.Forms.Button();
               this.menuEmployee = new System.Windows.Forms.Button();
               this.button1 = new System.Windows.Forms.Button();
               this.menuGroupBox = new System.Windows.Forms.GroupBox();
               this.shipmentButton = new System.Windows.Forms.Button();
               ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
               this.menuGroupBox.SuspendLayout();
               this.SuspendLayout();
               // 
               // addCustomerButton
               // 
               this.addCustomerButton.Location = new System.Drawing.Point(154, 41);
               this.addCustomerButton.Name = "addCustomerButton";
               this.addCustomerButton.Size = new System.Drawing.Size(116, 35);
               this.addCustomerButton.TabIndex = 0;
               this.addCustomerButton.Text = "Add Customer";
               this.addCustomerButton.UseVisualStyleBackColor = true;
               this.addCustomerButton.Click += new System.EventHandler(this.addCustomerButton_Click);
               // 
               // updateCustomerButton
               // 
               this.updateCustomerButton.Location = new System.Drawing.Point(294, 41);
               this.updateCustomerButton.Name = "updateCustomerButton";
               this.updateCustomerButton.Size = new System.Drawing.Size(108, 35);
               this.updateCustomerButton.TabIndex = 1;
               this.updateCustomerButton.Text = "Update Customer";
               this.updateCustomerButton.UseVisualStyleBackColor = true;
               this.updateCustomerButton.Click += new System.EventHandler(this.updateCustomerButton_Click);
               // 
               // removeCustomerButton
               // 
               this.removeCustomerButton.Location = new System.Drawing.Point(426, 41);
               this.removeCustomerButton.Name = "removeCustomerButton";
               this.removeCustomerButton.Size = new System.Drawing.Size(115, 35);
               this.removeCustomerButton.TabIndex = 2;
               this.removeCustomerButton.Text = "Remove Customer";
               this.removeCustomerButton.UseVisualStyleBackColor = true;
               this.removeCustomerButton.Click += new System.EventHandler(this.removeCustomerButton_Click);
               // 
               // customerFirstNameLabel
               // 
               this.customerFirstNameLabel.AutoSize = true;
               this.customerFirstNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.customerFirstNameLabel.Location = new System.Drawing.Point(300, 146);
               this.customerFirstNameLabel.Name = "customerFirstNameLabel";
               this.customerFirstNameLabel.Size = new System.Drawing.Size(90, 20);
               this.customerFirstNameLabel.TabIndex = 3;
               this.customerFirstNameLabel.Text = "First Name:";
               // 
               // customerLastNameLabel
               // 
               this.customerLastNameLabel.AutoSize = true;
               this.customerLastNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.customerLastNameLabel.Location = new System.Drawing.Point(446, 146);
               this.customerLastNameLabel.Name = "customerLastNameLabel";
               this.customerLastNameLabel.Size = new System.Drawing.Size(90, 20);
               this.customerLastNameLabel.TabIndex = 4;
               this.customerLastNameLabel.Text = "Last Name:";
               // 
               // firstNameText
               // 
               this.firstNameText.Location = new System.Drawing.Point(295, 170);
               this.firstNameText.Name = "firstNameText";
               this.firstNameText.Size = new System.Drawing.Size(100, 20);
               this.firstNameText.TabIndex = 5;
               // 
               // lastNameText
               // 
               this.lastNameText.Location = new System.Drawing.Point(440, 170);
               this.lastNameText.Name = "lastNameText";
               this.lastNameText.Size = new System.Drawing.Size(100, 20);
               this.lastNameText.TabIndex = 6;
               // 
               // customerPhoneNumberLabel
               // 
               this.customerPhoneNumberLabel.AutoSize = true;
               this.customerPhoneNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.customerPhoneNumberLabel.Location = new System.Drawing.Point(300, 218);
               this.customerPhoneNumberLabel.Name = "customerPhoneNumberLabel";
               this.customerPhoneNumberLabel.Size = new System.Drawing.Size(72, 20);
               this.customerPhoneNumberLabel.TabIndex = 7;
               this.customerPhoneNumberLabel.Text = "Phone #:";
               // 
               // phoneNumberText
               // 
               this.phoneNumberText.Location = new System.Drawing.Point(378, 218);
               this.phoneNumberText.Name = "phoneNumberText";
               this.phoneNumberText.Size = new System.Drawing.Size(84, 20);
               this.phoneNumberText.TabIndex = 8;
               // 
               // customerAddressLabel
               // 
               this.customerAddressLabel.AutoSize = true;
               this.customerAddressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.customerAddressLabel.Location = new System.Drawing.Point(385, 262);
               this.customerAddressLabel.Name = "customerAddressLabel";
               this.customerAddressLabel.Size = new System.Drawing.Size(72, 20);
               this.customerAddressLabel.TabIndex = 9;
               this.customerAddressLabel.Text = "Address:";
               // 
               // addressText
               // 
               this.addressText.Location = new System.Drawing.Point(295, 285);
               this.addressText.Name = "addressText";
               this.addressText.Size = new System.Drawing.Size(245, 20);
               this.addressText.TabIndex = 10;
               // 
               // submitButton
               // 
               this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.submitButton.Location = new System.Drawing.Point(382, 321);
               this.submitButton.Name = "submitButton";
               this.submitButton.Size = new System.Drawing.Size(75, 35);
               this.submitButton.TabIndex = 11;
               this.submitButton.Text = "Submit";
               this.submitButton.UseVisualStyleBackColor = true;
               this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
               // 
               // customerIdLabel
               // 
               this.customerIdLabel.AutoSize = true;
               this.customerIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.customerIdLabel.Location = new System.Drawing.Point(345, 109);
               this.customerIdLabel.Name = "customerIdLabel";
               this.customerIdLabel.Size = new System.Drawing.Size(27, 20);
               this.customerIdLabel.TabIndex = 12;
               this.customerIdLabel.Text = "Id:";
               // 
               // idText
               // 
               this.idText.Location = new System.Drawing.Point(378, 109);
               this.idText.Name = "idText";
               this.idText.Size = new System.Drawing.Size(84, 20);
               this.idText.TabIndex = 13;
               // 
               // dataGrid
               // 
               this.dataGrid.AllowUserToAddRows = false;
               this.dataGrid.AllowUserToDeleteRows = false;
               this.dataGrid.AllowUserToResizeColumns = false;
               this.dataGrid.AllowUserToResizeRows = false;
               this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
               this.dataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.customer_id,
            this.customer_first_name,
            this.customer_last_name,
            this.customer_phone_number,
            this.customer_address});
               this.dataGrid.Location = new System.Drawing.Point(143, 90);
               this.dataGrid.Name = "dataGrid";
               this.dataGrid.ReadOnly = true;
               this.dataGrid.Size = new System.Drawing.Size(543, 321);
               this.dataGrid.TabIndex = 14;
               // 
               // customer_id
               // 
               this.customer_id.DataPropertyName = "Id";
               this.customer_id.HeaderText = "Customer ID";
               this.customer_id.Name = "customer_id";
               this.customer_id.ReadOnly = true;
               // 
               // customer_first_name
               // 
               this.customer_first_name.DataPropertyName = "First_name";
               this.customer_first_name.HeaderText = "First Name";
               this.customer_first_name.Name = "customer_first_name";
               this.customer_first_name.ReadOnly = true;
               this.customer_first_name.Width = 101;
               // 
               // customer_last_name
               // 
               this.customer_last_name.DataPropertyName = "Last_name";
               this.customer_last_name.HeaderText = "Last Name";
               this.customer_last_name.Name = "customer_last_name";
               this.customer_last_name.ReadOnly = true;
               // 
               // customer_phone_number
               // 
               this.customer_phone_number.DataPropertyName = "Phone_";
               this.customer_phone_number.HeaderText = "Phone #";
               this.customer_phone_number.Name = "customer_phone_number";
               this.customer_phone_number.ReadOnly = true;
               this.customer_phone_number.Width = 101;
               // 
               // customer_address
               // 
               this.customer_address.DataPropertyName = "Address";
               this.customer_address.HeaderText = "Address";
               this.customer_address.Name = "customer_address";
               this.customer_address.ReadOnly = true;
               // 
               // viewCustomerButton
               // 
               this.viewCustomerButton.Location = new System.Drawing.Point(561, 41);
               this.viewCustomerButton.Name = "viewCustomerButton";
               this.viewCustomerButton.Size = new System.Drawing.Size(104, 35);
               this.viewCustomerButton.TabIndex = 15;
               this.viewCustomerButton.Text = "View Customer";
               this.viewCustomerButton.UseVisualStyleBackColor = true;
               this.viewCustomerButton.Click += new System.EventHandler(this.viewCustomerButton_Click);
               // 
               // menuPackage
               // 
               this.menuPackage.Location = new System.Drawing.Point(10, 60);
               this.menuPackage.Name = "menuPackage";
               this.menuPackage.Size = new System.Drawing.Size(77, 35);
               this.menuPackage.TabIndex = 43;
               this.menuPackage.Text = "Package";
               this.menuPackage.UseVisualStyleBackColor = true;
               this.menuPackage.Click += new System.EventHandler(this.menuPackage_Click);
               // 
               // menuEmployee
               // 
               this.menuEmployee.Location = new System.Drawing.Point(10, 19);
               this.menuEmployee.Name = "menuEmployee";
               this.menuEmployee.Size = new System.Drawing.Size(77, 35);
               this.menuEmployee.TabIndex = 44;
               this.menuEmployee.Text = "Employee";
               this.menuEmployee.UseVisualStyleBackColor = true;
               this.menuEmployee.Click += new System.EventHandler(this.menuEmployee_Click);
               // 
               // button1
               // 
               this.button1.BackColor = System.Drawing.Color.Red;
               this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
               this.button1.Location = new System.Drawing.Point(10, 143);
               this.button1.Name = "button1";
               this.button1.Size = new System.Drawing.Size(77, 35);
               this.button1.TabIndex = 45;
               this.button1.Text = "EXIT";
               this.button1.UseVisualStyleBackColor = false;
               this.button1.Click += new System.EventHandler(this.button1_Click);
               // 
               // menuGroupBox
               // 
               this.menuGroupBox.BackColor = System.Drawing.Color.Yellow;
               this.menuGroupBox.Controls.Add(this.shipmentButton);
               this.menuGroupBox.Controls.Add(this.button1);
               this.menuGroupBox.Controls.Add(this.menuPackage);
               this.menuGroupBox.Controls.Add(this.menuEmployee);
               this.menuGroupBox.Location = new System.Drawing.Point(12, 41);
               this.menuGroupBox.Name = "menuGroupBox";
               this.menuGroupBox.Size = new System.Drawing.Size(103, 188);
               this.menuGroupBox.TabIndex = 47;
               this.menuGroupBox.TabStop = false;
               this.menuGroupBox.Text = "Menu";
               // 
               // shipmentButton
               // 
               this.shipmentButton.Location = new System.Drawing.Point(10, 102);
               this.shipmentButton.Name = "shipmentButton";
               this.shipmentButton.Size = new System.Drawing.Size(77, 35);
               this.shipmentButton.TabIndex = 45;
               this.shipmentButton.Text = "Shipment";
               this.shipmentButton.UseVisualStyleBackColor = true;
               this.shipmentButton.Click += new System.EventHandler(this.shipmentButton_Click);
               // 
               // CustomerForm
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.BackColor = System.Drawing.Color.Yellow;
               this.ClientSize = new System.Drawing.Size(800, 450);
               this.Controls.Add(this.viewCustomerButton);
               this.Controls.Add(this.dataGrid);
               this.Controls.Add(this.idText);
               this.Controls.Add(this.customerIdLabel);
               this.Controls.Add(this.submitButton);
               this.Controls.Add(this.addressText);
               this.Controls.Add(this.customerAddressLabel);
               this.Controls.Add(this.phoneNumberText);
               this.Controls.Add(this.customerPhoneNumberLabel);
               this.Controls.Add(this.lastNameText);
               this.Controls.Add(this.firstNameText);
               this.Controls.Add(this.customerLastNameLabel);
               this.Controls.Add(this.customerFirstNameLabel);
               this.Controls.Add(this.removeCustomerButton);
               this.Controls.Add(this.updateCustomerButton);
               this.Controls.Add(this.addCustomerButton);
               this.Controls.Add(this.menuGroupBox);
               this.Name = "CustomerForm";
               this.Text = "CustomerForm";
               this.Load += new System.EventHandler(this.CustomerForm_Load);
               ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
               this.menuGroupBox.ResumeLayout(false);
               this.ResumeLayout(false);
               this.PerformLayout();

          }

          #endregion

          private System.Windows.Forms.Button addCustomerButton;
          private System.Windows.Forms.Button updateCustomerButton;
          private System.Windows.Forms.Button removeCustomerButton;
          private System.Windows.Forms.Label customerFirstNameLabel;
          private System.Windows.Forms.Label customerLastNameLabel;
          private System.Windows.Forms.TextBox firstNameText;
          private System.Windows.Forms.TextBox lastNameText;
          private System.Windows.Forms.Label customerPhoneNumberLabel;
          private System.Windows.Forms.TextBox phoneNumberText;
          private System.Windows.Forms.Label customerAddressLabel;
          private System.Windows.Forms.TextBox addressText;
          private System.Windows.Forms.Button submitButton;
          private System.Windows.Forms.Label customerIdLabel;
          private System.Windows.Forms.TextBox idText;
          private System.Windows.Forms.DataGridView dataGrid;
          private System.Windows.Forms.Button viewCustomerButton;
          private System.Windows.Forms.DataGridViewTextBoxColumn customer_id;
          private System.Windows.Forms.DataGridViewTextBoxColumn customer_first_name;
          private System.Windows.Forms.DataGridViewTextBoxColumn customer_last_name;
          private System.Windows.Forms.DataGridViewTextBoxColumn customer_phone_number;
          private System.Windows.Forms.DataGridViewTextBoxColumn customer_address;
          private System.Windows.Forms.Button menuPackage;
          private System.Windows.Forms.Button menuEmployee;
          private System.Windows.Forms.Button button1;
          private System.Windows.Forms.GroupBox menuGroupBox;
          private System.Windows.Forms.Button shipmentButton;
     }
}