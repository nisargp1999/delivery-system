﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryService
{
    public partial class Package
    {
        public string Package_Information => $"Package ID:{Id}      Customer Name: {Customer.First_name} {Customer.Last_name}";
        public string Better_FirstName => $"{Customer.First_name}";
        public string Better_LastName => $"{Customer.Last_name}";
    }
    public partial class Building
    {
        public string Building_Information => $"Building ID: {Id}   Building Address: {Address}";
    }
}
