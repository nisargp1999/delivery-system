﻿namespace DeliveryService
{
     partial class PackageForm
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.components = new System.ComponentModel.Container();
               this.viewPackageButton = new System.Windows.Forms.Button();
               this.removePackageButton = new System.Windows.Forms.Button();
               this.updatePackageButton = new System.Windows.Forms.Button();
               this.addPackageButton = new System.Windows.Forms.Button();
               this.idText = new System.Windows.Forms.TextBox();
               this.packageIdLabel = new System.Windows.Forms.Label();
               this.packageLengthLabel = new System.Windows.Forms.Label();
               this.packageWidthLabel = new System.Windows.Forms.Label();
               this.packageHeightLabel = new System.Windows.Forms.Label();
               this.packageWeightLabel = new System.Windows.Forms.Label();
               this.shipperIdLabel = new System.Windows.Forms.Label();
               this.packageDestinationLabel = new System.Windows.Forms.Label();
               this.destinationText = new System.Windows.Forms.TextBox();
               this.submitButton = new System.Windows.Forms.Button();
               this.customerListBox = new System.Windows.Forms.ListBox();
               this.lengthText = new System.Windows.Forms.TextBox();
               this.widthText = new System.Windows.Forms.TextBox();
               this.heightText = new System.Windows.Forms.TextBox();
               this.weightText = new System.Windows.Forms.TextBox();
               this.updateCheckBox = new System.Windows.Forms.CheckBox();
               this.packageDataGrid = new System.Windows.Forms.DataGridView();
               this.package_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.package_length = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.package_width = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.package_height = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.package_weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.package_destination = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.package_shipper_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
               this.menuCustomer = new System.Windows.Forms.Button();
               this.menuEmployee = new System.Windows.Forms.Button();
               this.button1 = new System.Windows.Forms.Button();
               this.menuGroupBox = new System.Windows.Forms.GroupBox();
               this.shipmentButton = new System.Windows.Forms.Button();
               this.customerBindingSource = new System.Windows.Forms.BindingSource(this.components);
               ((System.ComponentModel.ISupportInitialize)(this.packageDataGrid)).BeginInit();
               this.menuGroupBox.SuspendLayout();
               ((System.ComponentModel.ISupportInitialize)(this.customerBindingSource)).BeginInit();
               this.SuspendLayout();
               // 
               // viewPackageButton
               // 
               this.viewPackageButton.Location = new System.Drawing.Point(600, 29);
               this.viewPackageButton.Name = "viewPackageButton";
               this.viewPackageButton.Size = new System.Drawing.Size(104, 35);
               this.viewPackageButton.TabIndex = 19;
               this.viewPackageButton.Text = "View Packages";
               this.viewPackageButton.UseVisualStyleBackColor = true;
               this.viewPackageButton.Click += new System.EventHandler(this.viewPackageButton_Click);
               // 
               // removePackageButton
               // 
               this.removePackageButton.Location = new System.Drawing.Point(465, 29);
               this.removePackageButton.Name = "removePackageButton";
               this.removePackageButton.Size = new System.Drawing.Size(115, 35);
               this.removePackageButton.TabIndex = 18;
               this.removePackageButton.Text = "Remove Package";
               this.removePackageButton.UseVisualStyleBackColor = true;
               this.removePackageButton.Click += new System.EventHandler(this.removePackageButton_Click);
               // 
               // updatePackageButton
               // 
               this.updatePackageButton.Location = new System.Drawing.Point(333, 29);
               this.updatePackageButton.Name = "updatePackageButton";
               this.updatePackageButton.Size = new System.Drawing.Size(108, 35);
               this.updatePackageButton.TabIndex = 17;
               this.updatePackageButton.Text = "Update Package";
               this.updatePackageButton.UseVisualStyleBackColor = true;
               this.updatePackageButton.Click += new System.EventHandler(this.updatePackageButton_Click);
               // 
               // addPackageButton
               // 
               this.addPackageButton.Location = new System.Drawing.Point(193, 29);
               this.addPackageButton.Name = "addPackageButton";
               this.addPackageButton.Size = new System.Drawing.Size(116, 35);
               this.addPackageButton.TabIndex = 16;
               this.addPackageButton.Text = "Add Package";
               this.addPackageButton.UseVisualStyleBackColor = true;
               this.addPackageButton.Click += new System.EventHandler(this.addPackageButton_Click);
               // 
               // idText
               // 
               this.idText.Location = new System.Drawing.Point(431, 98);
               this.idText.Name = "idText";
               this.idText.Size = new System.Drawing.Size(84, 20);
               this.idText.TabIndex = 21;
               // 
               // packageIdLabel
               // 
               this.packageIdLabel.AutoSize = true;
               this.packageIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.packageIdLabel.Location = new System.Drawing.Point(337, 98);
               this.packageIdLabel.Name = "packageIdLabel";
               this.packageIdLabel.Size = new System.Drawing.Size(93, 20);
               this.packageIdLabel.TabIndex = 20;
               this.packageIdLabel.Text = "Package Id:";
               // 
               // packageLengthLabel
               // 
               this.packageLengthLabel.AutoSize = true;
               this.packageLengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.packageLengthLabel.Location = new System.Drawing.Point(266, 132);
               this.packageLengthLabel.Name = "packageLengthLabel";
               this.packageLengthLabel.Size = new System.Drawing.Size(63, 20);
               this.packageLengthLabel.TabIndex = 22;
               this.packageLengthLabel.Text = "Length:";
               // 
               // packageWidthLabel
               // 
               this.packageWidthLabel.AutoSize = true;
               this.packageWidthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.packageWidthLabel.Location = new System.Drawing.Point(369, 132);
               this.packageWidthLabel.Name = "packageWidthLabel";
               this.packageWidthLabel.Size = new System.Drawing.Size(54, 20);
               this.packageWidthLabel.TabIndex = 23;
               this.packageWidthLabel.Text = "Width:";
               // 
               // packageHeightLabel
               // 
               this.packageHeightLabel.AutoSize = true;
               this.packageHeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.packageHeightLabel.Location = new System.Drawing.Point(458, 132);
               this.packageHeightLabel.Name = "packageHeightLabel";
               this.packageHeightLabel.Size = new System.Drawing.Size(60, 20);
               this.packageHeightLabel.TabIndex = 24;
               this.packageHeightLabel.Text = "Height:";
               // 
               // packageWeightLabel
               // 
               this.packageWeightLabel.AutoSize = true;
               this.packageWeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.packageWeightLabel.Location = new System.Drawing.Point(540, 132);
               this.packageWeightLabel.Name = "packageWeightLabel";
               this.packageWeightLabel.Size = new System.Drawing.Size(63, 20);
               this.packageWeightLabel.TabIndex = 28;
               this.packageWeightLabel.Text = "Weight:";
               // 
               // shipperIdLabel
               // 
               this.shipperIdLabel.AutoSize = true;
               this.shipperIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.shipperIdLabel.Location = new System.Drawing.Point(266, 244);
               this.shipperIdLabel.Name = "shipperIdLabel";
               this.shipperIdLabel.Size = new System.Drawing.Size(335, 20);
               this.shipperIdLabel.TabIndex = 30;
               this.shipperIdLabel.Text = "Please select shipper id from the list box below";
               // 
               // packageDestinationLabel
               // 
               this.packageDestinationLabel.AutoSize = true;
               this.packageDestinationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.packageDestinationLabel.Location = new System.Drawing.Point(249, 194);
               this.packageDestinationLabel.Name = "packageDestinationLabel";
               this.packageDestinationLabel.Size = new System.Drawing.Size(157, 20);
               this.packageDestinationLabel.TabIndex = 32;
               this.packageDestinationLabel.Text = "Destination Address:";
               // 
               // destinationText
               // 
               this.destinationText.Location = new System.Drawing.Point(412, 194);
               this.destinationText.Name = "destinationText";
               this.destinationText.Size = new System.Drawing.Size(208, 20);
               this.destinationText.TabIndex = 33;
               // 
               // submitButton
               // 
               this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.submitButton.Location = new System.Drawing.Point(386, 385);
               this.submitButton.Name = "submitButton";
               this.submitButton.Size = new System.Drawing.Size(75, 35);
               this.submitButton.TabIndex = 34;
               this.submitButton.Text = "Submit";
               this.submitButton.UseVisualStyleBackColor = true;
               this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
               // 
               // customerListBox
               // 
               this.customerListBox.FormattingEnabled = true;
               this.customerListBox.Location = new System.Drawing.Point(208, 267);
               this.customerListBox.Name = "customerListBox";
               this.customerListBox.Size = new System.Drawing.Size(446, 95);
               this.customerListBox.TabIndex = 35;
               // 
               // lengthText
               // 
               this.lengthText.Location = new System.Drawing.Point(270, 155);
               this.lengthText.Name = "lengthText";
               this.lengthText.Size = new System.Drawing.Size(59, 20);
               this.lengthText.TabIndex = 36;
               // 
               // widthText
               // 
               this.widthText.Location = new System.Drawing.Point(373, 155);
               this.widthText.Name = "widthText";
               this.widthText.Size = new System.Drawing.Size(50, 20);
               this.widthText.TabIndex = 37;
               // 
               // heightText
               // 
               this.heightText.Location = new System.Drawing.Point(456, 155);
               this.heightText.Name = "heightText";
               this.heightText.Size = new System.Drawing.Size(59, 20);
               this.heightText.TabIndex = 38;
               // 
               // weightText
               // 
               this.weightText.Location = new System.Drawing.Point(542, 155);
               this.weightText.Name = "weightText";
               this.weightText.Size = new System.Drawing.Size(59, 20);
               this.weightText.TabIndex = 39;
               // 
               // updateCheckBox
               // 
               this.updateCheckBox.AutoSize = true;
               this.updateCheckBox.Location = new System.Drawing.Point(208, 244);
               this.updateCheckBox.Name = "updateCheckBox";
               this.updateCheckBox.Size = new System.Drawing.Size(297, 17);
               this.updateCheckBox.TabIndex = 40;
               this.updateCheckBox.Text = "Please select this checkbox if you want to update shipper";
               this.updateCheckBox.UseVisualStyleBackColor = true;
               // 
               // packageDataGrid
               // 
               this.packageDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
               this.packageDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.package_id,
            this.package_length,
            this.package_width,
            this.package_height,
            this.package_weight,
            this.package_destination,
            this.package_shipper_id});
               this.packageDataGrid.Location = new System.Drawing.Point(132, 74);
               this.packageDataGrid.Name = "packageDataGrid";
               this.packageDataGrid.Size = new System.Drawing.Size(645, 288);
               this.packageDataGrid.TabIndex = 41;
               // 
               // package_id
               // 
               this.package_id.DataPropertyName = "Id";
               this.package_id.HeaderText = "Package Id";
               this.package_id.Name = "package_id";
               // 
               // package_length
               // 
               this.package_length.DataPropertyName = "Length";
               this.package_length.HeaderText = "Length";
               this.package_length.Name = "package_length";
               // 
               // package_width
               // 
               this.package_width.DataPropertyName = "Width";
               this.package_width.HeaderText = "Width";
               this.package_width.Name = "package_width";
               // 
               // package_height
               // 
               this.package_height.DataPropertyName = "Height";
               this.package_height.HeaderText = "Height";
               this.package_height.Name = "package_height";
               // 
               // package_weight
               // 
               this.package_weight.DataPropertyName = "Weight";
               this.package_weight.HeaderText = "Weight";
               this.package_weight.Name = "package_weight";
               // 
               // package_destination
               // 
               this.package_destination.DataPropertyName = "Destination";
               this.package_destination.HeaderText = "Destination";
               this.package_destination.Name = "package_destination";
               // 
               // package_shipper_id
               // 
               this.package_shipper_id.DataPropertyName = "Shipper_id";
               this.package_shipper_id.HeaderText = "Shipper Id";
               this.package_shipper_id.Name = "package_shipper_id";
               // 
               // menuCustomer
               // 
               this.menuCustomer.Location = new System.Drawing.Point(23, 49);
               this.menuCustomer.Name = "menuCustomer";
               this.menuCustomer.Size = new System.Drawing.Size(77, 35);
               this.menuCustomer.TabIndex = 42;
               this.menuCustomer.Text = "Customer";
               this.menuCustomer.UseVisualStyleBackColor = true;
               this.menuCustomer.Click += new System.EventHandler(this.menuCustomer_Click);
               // 
               // menuEmployee
               // 
               this.menuEmployee.Location = new System.Drawing.Point(11, 62);
               this.menuEmployee.Name = "menuEmployee";
               this.menuEmployee.Size = new System.Drawing.Size(77, 35);
               this.menuEmployee.TabIndex = 43;
               this.menuEmployee.Text = "Employee";
               this.menuEmployee.UseVisualStyleBackColor = true;
               this.menuEmployee.Click += new System.EventHandler(this.menuEmployee_Click);
               // 
               // button1
               // 
               this.button1.BackColor = System.Drawing.Color.Red;
               this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
               this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
               this.button1.Location = new System.Drawing.Point(11, 145);
               this.button1.Name = "button1";
               this.button1.Size = new System.Drawing.Size(77, 35);
               this.button1.TabIndex = 47;
               this.button1.Text = "EXIT";
               this.button1.UseVisualStyleBackColor = false;
               this.button1.Click += new System.EventHandler(this.button1_Click);
               // 
               // menuGroupBox
               // 
               this.menuGroupBox.Controls.Add(this.shipmentButton);
               this.menuGroupBox.Controls.Add(this.menuEmployee);
               this.menuGroupBox.Controls.Add(this.button1);
               this.menuGroupBox.Location = new System.Drawing.Point(12, 29);
               this.menuGroupBox.Name = "menuGroupBox";
               this.menuGroupBox.Size = new System.Drawing.Size(103, 188);
               this.menuGroupBox.TabIndex = 48;
               this.menuGroupBox.TabStop = false;
               this.menuGroupBox.Text = "Menu";
               // 
               // shipmentButton
               // 
               this.shipmentButton.Location = new System.Drawing.Point(11, 104);
               this.shipmentButton.Name = "shipmentButton";
               this.shipmentButton.Size = new System.Drawing.Size(77, 35);
               this.shipmentButton.TabIndex = 49;
               this.shipmentButton.Text = "Shipment";
               this.shipmentButton.UseVisualStyleBackColor = true;
               this.shipmentButton.Click += new System.EventHandler(this.shipmentButton_Click);
               // 
               // customerBindingSource
               // 
               this.customerBindingSource.DataSource = typeof(DeliveryService.Customer);
               // 
               // PackageForm
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.BackColor = System.Drawing.Color.Cyan;
               this.ClientSize = new System.Drawing.Size(800, 450);
               this.Controls.Add(this.menuCustomer);
               this.Controls.Add(this.packageDataGrid);
               this.Controls.Add(this.updateCheckBox);
               this.Controls.Add(this.weightText);
               this.Controls.Add(this.heightText);
               this.Controls.Add(this.widthText);
               this.Controls.Add(this.lengthText);
               this.Controls.Add(this.customerListBox);
               this.Controls.Add(this.submitButton);
               this.Controls.Add(this.destinationText);
               this.Controls.Add(this.packageDestinationLabel);
               this.Controls.Add(this.shipperIdLabel);
               this.Controls.Add(this.packageWeightLabel);
               this.Controls.Add(this.packageHeightLabel);
               this.Controls.Add(this.packageWidthLabel);
               this.Controls.Add(this.packageLengthLabel);
               this.Controls.Add(this.idText);
               this.Controls.Add(this.packageIdLabel);
               this.Controls.Add(this.viewPackageButton);
               this.Controls.Add(this.removePackageButton);
               this.Controls.Add(this.updatePackageButton);
               this.Controls.Add(this.addPackageButton);
               this.Controls.Add(this.menuGroupBox);
               this.Name = "PackageForm";
               this.Text = "PackageForm";
               this.Load += new System.EventHandler(this.PackageForm_Load);
               ((System.ComponentModel.ISupportInitialize)(this.packageDataGrid)).EndInit();
               this.menuGroupBox.ResumeLayout(false);
               ((System.ComponentModel.ISupportInitialize)(this.customerBindingSource)).EndInit();
               this.ResumeLayout(false);
               this.PerformLayout();

          }

          #endregion

          private System.Windows.Forms.Button viewPackageButton;
          private System.Windows.Forms.Button removePackageButton;
          private System.Windows.Forms.Button updatePackageButton;
          private System.Windows.Forms.Button addPackageButton;
          private System.Windows.Forms.TextBox idText;
          private System.Windows.Forms.Label packageIdLabel;
          private System.Windows.Forms.Label packageLengthLabel;
          private System.Windows.Forms.Label packageWidthLabel;
          private System.Windows.Forms.Label packageHeightLabel;
          private System.Windows.Forms.Label packageWeightLabel;
          private System.Windows.Forms.Label shipperIdLabel;
          private System.Windows.Forms.Label packageDestinationLabel;
          private System.Windows.Forms.TextBox destinationText;
          private System.Windows.Forms.Button submitButton;
          private System.Windows.Forms.BindingSource customerBindingSource;
          private System.Windows.Forms.ListBox customerListBox;
          private System.Windows.Forms.TextBox lengthText;
          private System.Windows.Forms.TextBox widthText;
          private System.Windows.Forms.TextBox heightText;
          private System.Windows.Forms.TextBox weightText;
          private System.Windows.Forms.CheckBox updateCheckBox;
          private System.Windows.Forms.DataGridView packageDataGrid;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_id;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_length;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_width;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_height;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_weight;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_destination;
          private System.Windows.Forms.DataGridViewTextBoxColumn package_shipper_id;
          private System.Windows.Forms.Button menuCustomer;
          private System.Windows.Forms.Button menuEmployee;
          private System.Windows.Forms.Button button1;
          private System.Windows.Forms.GroupBox menuGroupBox;
          private System.Windows.Forms.Button shipmentButton;
     }
}