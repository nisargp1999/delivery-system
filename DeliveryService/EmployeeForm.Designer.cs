﻿namespace DeliveryService
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addBtn = new System.Windows.Forms.Button();
            this.removeBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.viewBtn = new System.Windows.Forms.Button();
            this.vBuildBtn = new System.Windows.Forms.Button();
            this.menuBox = new System.Windows.Forms.GroupBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.shipmentBtn = new System.Windows.Forms.Button();
            this.customerBtn = new System.Windows.Forms.Button();
            this.packageBtn = new System.Windows.Forms.Button();
            this.ssnBox = new System.Windows.Forms.TextBox();
            this.fnameBox = new System.Windows.Forms.TextBox();
            this.lnameBox = new System.Windows.Forms.TextBox();
            this.addrBox = new System.Windows.Forms.TextBox();
            this.phone1Box = new System.Windows.Forms.TextBox();
            this.phone2Box = new System.Windows.Forms.TextBox();
            this.buildLBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.submitBtn = new System.Windows.Forms.Button();
            this.empBox = new System.Windows.Forms.DataGridView();
            this.bBox = new System.Windows.Forms.DataGridView();
            this.buildingID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOpen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bClose = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SocialSecurity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Addr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.empBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bBox)).BeginInit();
            this.SuspendLayout();
            // 
            // addBtn
            // 
            this.addBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBtn.Location = new System.Drawing.Point(168, 22);
            this.addBtn.Margin = new System.Windows.Forms.Padding(2);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(104, 35);
            this.addBtn.TabIndex = 0;
            this.addBtn.Text = "Add Employee";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeBtn.Location = new System.Drawing.Point(280, 23);
            this.removeBtn.Margin = new System.Windows.Forms.Padding(2);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(104, 35);
            this.removeBtn.TabIndex = 1;
            this.removeBtn.Text = "Remove Employee";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // updateBtn
            // 
            this.updateBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateBtn.Location = new System.Drawing.Point(403, 22);
            this.updateBtn.Margin = new System.Windows.Forms.Padding(2);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(104, 35);
            this.updateBtn.TabIndex = 2;
            this.updateBtn.Text = "Update Employee";
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // viewBtn
            // 
            this.viewBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewBtn.Location = new System.Drawing.Point(535, 23);
            this.viewBtn.Margin = new System.Windows.Forms.Padding(2);
            this.viewBtn.Name = "viewBtn";
            this.viewBtn.Size = new System.Drawing.Size(104, 35);
            this.viewBtn.TabIndex = 3;
            this.viewBtn.Text = "View Employees";
            this.viewBtn.UseVisualStyleBackColor = true;
            this.viewBtn.Click += new System.EventHandler(this.viewBtn_Click);
            // 
            // vBuildBtn
            // 
            this.vBuildBtn.Location = new System.Drawing.Point(657, 23);
            this.vBuildBtn.Margin = new System.Windows.Forms.Padding(2);
            this.vBuildBtn.Name = "vBuildBtn";
            this.vBuildBtn.Size = new System.Drawing.Size(104, 35);
            this.vBuildBtn.TabIndex = 4;
            this.vBuildBtn.Text = "View Buildings";
            this.vBuildBtn.UseVisualStyleBackColor = true;
            this.vBuildBtn.Click += new System.EventHandler(this.vBuildBtn_Click);
            // 
            // menuBox
            // 
            this.menuBox.Controls.Add(this.exitBtn);
            this.menuBox.Controls.Add(this.shipmentBtn);
            this.menuBox.Controls.Add(this.customerBtn);
            this.menuBox.Controls.Add(this.packageBtn);
            this.menuBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.menuBox.Location = new System.Drawing.Point(2, 10);
            this.menuBox.Margin = new System.Windows.Forms.Padding(2);
            this.menuBox.Name = "menuBox";
            this.menuBox.Padding = new System.Windows.Forms.Padding(2);
            this.menuBox.Size = new System.Drawing.Size(103, 188);
            this.menuBox.TabIndex = 5;
            this.menuBox.TabStop = false;
            this.menuBox.Text = "Menu";
            this.menuBox.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.Red;
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.Location = new System.Drawing.Point(4, 141);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(77, 35);
            this.exitBtn.TabIndex = 23;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // shipmentBtn
            // 
            this.shipmentBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shipmentBtn.Location = new System.Drawing.Point(4, 100);
            this.shipmentBtn.Margin = new System.Windows.Forms.Padding(2);
            this.shipmentBtn.Name = "shipmentBtn";
            this.shipmentBtn.Size = new System.Drawing.Size(77, 35);
            this.shipmentBtn.TabIndex = 9;
            this.shipmentBtn.Text = "Shipment";
            this.shipmentBtn.UseVisualStyleBackColor = true;
            this.shipmentBtn.Click += new System.EventHandler(this.shipmentBtn_Click);
            // 
            // customerBtn
            // 
            this.customerBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerBtn.Location = new System.Drawing.Point(4, 15);
            this.customerBtn.Margin = new System.Windows.Forms.Padding(2);
            this.customerBtn.Name = "customerBtn";
            this.customerBtn.Size = new System.Drawing.Size(77, 35);
            this.customerBtn.TabIndex = 6;
            this.customerBtn.Text = "Customer";
            this.customerBtn.UseVisualStyleBackColor = true;
            this.customerBtn.Click += new System.EventHandler(this.customerBtn_Click);
            // 
            // packageBtn
            // 
            this.packageBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packageBtn.Location = new System.Drawing.Point(4, 58);
            this.packageBtn.Margin = new System.Windows.Forms.Padding(2);
            this.packageBtn.Name = "packageBtn";
            this.packageBtn.Size = new System.Drawing.Size(77, 35);
            this.packageBtn.TabIndex = 8;
            this.packageBtn.Text = "Package";
            this.packageBtn.UseVisualStyleBackColor = true;
            this.packageBtn.Click += new System.EventHandler(this.packageBtn_Click);
            // 
            // ssnBox
            // 
            this.ssnBox.Location = new System.Drawing.Point(226, 76);
            this.ssnBox.Margin = new System.Windows.Forms.Padding(2);
            this.ssnBox.Name = "ssnBox";
            this.ssnBox.Size = new System.Drawing.Size(96, 20);
            this.ssnBox.TabIndex = 6;
            this.ssnBox.TextChanged += new System.EventHandler(this.ssnBox_TextChanged);
            // 
            // fnameBox
            // 
            this.fnameBox.Location = new System.Drawing.Point(226, 110);
            this.fnameBox.Margin = new System.Windows.Forms.Padding(2);
            this.fnameBox.Name = "fnameBox";
            this.fnameBox.Size = new System.Drawing.Size(96, 20);
            this.fnameBox.TabIndex = 7;
            this.fnameBox.TextChanged += new System.EventHandler(this.fnameBox_TextChanged);
            // 
            // lnameBox
            // 
            this.lnameBox.Location = new System.Drawing.Point(388, 110);
            this.lnameBox.Margin = new System.Windows.Forms.Padding(2);
            this.lnameBox.Name = "lnameBox";
            this.lnameBox.Size = new System.Drawing.Size(96, 20);
            this.lnameBox.TabIndex = 8;
            this.lnameBox.TextChanged += new System.EventHandler(this.lnameBox_TextChanged);
            // 
            // addrBox
            // 
            this.addrBox.Location = new System.Drawing.Point(226, 145);
            this.addrBox.Margin = new System.Windows.Forms.Padding(2);
            this.addrBox.Name = "addrBox";
            this.addrBox.Size = new System.Drawing.Size(196, 20);
            this.addrBox.TabIndex = 9;
            this.addrBox.TextChanged += new System.EventHandler(this.addrBox_TextChanged);
            // 
            // phone1Box
            // 
            this.phone1Box.Location = new System.Drawing.Point(226, 173);
            this.phone1Box.Margin = new System.Windows.Forms.Padding(2);
            this.phone1Box.Name = "phone1Box";
            this.phone1Box.Size = new System.Drawing.Size(196, 20);
            this.phone1Box.TabIndex = 10;
            this.phone1Box.TextChanged += new System.EventHandler(this.phone1Box_TextChanged);
            // 
            // phone2Box
            // 
            this.phone2Box.Location = new System.Drawing.Point(226, 204);
            this.phone2Box.Margin = new System.Windows.Forms.Padding(2);
            this.phone2Box.Name = "phone2Box";
            this.phone2Box.Size = new System.Drawing.Size(196, 20);
            this.phone2Box.TabIndex = 11;
            this.phone2Box.TextChanged += new System.EventHandler(this.phone2Box_TextChanged);
            // 
            // buildLBox
            // 
            this.buildLBox.FormattingEnabled = true;
            this.buildLBox.Location = new System.Drawing.Point(226, 237);
            this.buildLBox.Margin = new System.Windows.Forms.Padding(2);
            this.buildLBox.Name = "buildLBox";
            this.buildLBox.Size = new System.Drawing.Size(258, 108);
            this.buildLBox.TabIndex = 12;
            this.buildLBox.SelectedIndexChanged += new System.EventHandler(this.buildLBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "ID";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(165, 114);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "First Name";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(326, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Last Name";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Address";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(165, 173);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Phone #1";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(165, 204);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Phone #2";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(165, 237);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Building";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // submitBtn
            // 
            this.submitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitBtn.Location = new System.Drawing.Point(535, 318);
            this.submitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(75, 35);
            this.submitBtn.TabIndex = 20;
            this.submitBtn.Text = "Submit";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // empBox
            // 
            this.empBox.AllowUserToAddRows = false;
            this.empBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.empBox.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SocialSecurity,
            this.FirstN,
            this.LastN,
            this.Addr,
            this.Phone1,
            this.Phone2,
            this.BID});
            this.empBox.Location = new System.Drawing.Point(145, 76);
            this.empBox.Margin = new System.Windows.Forms.Padding(2);
            this.empBox.Name = "empBox";
            this.empBox.ReadOnly = true;
            this.empBox.RowTemplate.Height = 24;
            this.empBox.Size = new System.Drawing.Size(616, 223);
            this.empBox.TabIndex = 21;
            this.empBox.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // bBox
            // 
            this.bBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bBox.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.buildingID,
            this.bAddress,
            this.bOpen,
            this.bClose});
            this.bBox.Location = new System.Drawing.Point(145, 76);
            this.bBox.Margin = new System.Windows.Forms.Padding(2);
            this.bBox.Name = "bBox";
            this.bBox.RowTemplate.Height = 24;
            this.bBox.Size = new System.Drawing.Size(616, 223);
            this.bBox.TabIndex = 22;
            this.bBox.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bBox_CellContentClick);
            // 
            // buildingID
            // 
            this.buildingID.DataPropertyName = "Id";
            this.buildingID.HeaderText = "Building ID";
            this.buildingID.Name = "buildingID";
            // 
            // bAddress
            // 
            this.bAddress.DataPropertyName = "Address";
            this.bAddress.HeaderText = "Address";
            this.bAddress.Name = "bAddress";
            // 
            // bOpen
            // 
            this.bOpen.DataPropertyName = "Open_time";
            this.bOpen.HeaderText = "Open";
            this.bOpen.Name = "bOpen";
            // 
            // bClose
            // 
            this.bClose.DataPropertyName = "Close_time";
            this.bClose.HeaderText = "Close";
            this.bClose.Name = "bClose";
            // 
            // SocialSecurity
            // 
            this.SocialSecurity.DataPropertyName = "Id";
            this.SocialSecurity.HeaderText = "ID";
            this.SocialSecurity.Name = "SocialSecurity";
            this.SocialSecurity.ReadOnly = true;
            // 
            // FirstN
            // 
            this.FirstN.DataPropertyName = "First_Name";
            this.FirstN.HeaderText = "First Name";
            this.FirstN.Name = "FirstN";
            this.FirstN.ReadOnly = true;
            // 
            // LastN
            // 
            this.LastN.DataPropertyName = "Last_Name";
            this.LastN.HeaderText = "Last Name";
            this.LastN.Name = "LastN";
            this.LastN.ReadOnly = true;
            // 
            // Addr
            // 
            this.Addr.DataPropertyName = "Address";
            this.Addr.HeaderText = "Address";
            this.Addr.Name = "Addr";
            this.Addr.ReadOnly = true;
            // 
            // Phone1
            // 
            this.Phone1.DataPropertyName = "Phone_";
            this.Phone1.HeaderText = "Phone Number";
            this.Phone1.Name = "Phone1";
            this.Phone1.ReadOnly = true;
            // 
            // Phone2
            // 
            this.Phone2.DataPropertyName = "Phone__2";
            this.Phone2.HeaderText = "Phone Number 2";
            this.Phone2.Name = "Phone2";
            this.Phone2.ReadOnly = true;
            // 
            // BID
            // 
            this.BID.DataPropertyName = "Building_ID";
            this.BID.HeaderText = "Building ID";
            this.BID.Name = "BID";
            this.BID.ReadOnly = true;
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.vBuildBtn);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.viewBtn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.updateBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buildLBox);
            this.Controls.Add(this.phone2Box);
            this.Controls.Add(this.phone1Box);
            this.Controls.Add(this.addrBox);
            this.Controls.Add(this.lnameBox);
            this.Controls.Add(this.fnameBox);
            this.Controls.Add(this.ssnBox);
            this.Controls.Add(this.menuBox);
            this.Controls.Add(this.bBox);
            this.Controls.Add(this.empBox);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "EmployeeForm";
            this.Text = "Employee";
            this.menuBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.empBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Button viewBtn;
        private System.Windows.Forms.GroupBox menuBox;
        private System.Windows.Forms.Button shipmentBtn;
        private System.Windows.Forms.Button customerBtn;
        private System.Windows.Forms.Button packageBtn;
        private System.Windows.Forms.TextBox ssnBox;
        private System.Windows.Forms.TextBox fnameBox;
        private System.Windows.Forms.TextBox lnameBox;
        private System.Windows.Forms.TextBox addrBox;
        private System.Windows.Forms.TextBox phone1Box;
        private System.Windows.Forms.TextBox phone2Box;
        private System.Windows.Forms.ListBox buildLBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.DataGridView empBox;
        private System.Windows.Forms.Button vBuildBtn;
        private System.Windows.Forms.DataGridView bBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn buildingID;
        private System.Windows.Forms.DataGridViewTextBoxColumn bAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn bOpen;
        private System.Windows.Forms.DataGridViewTextBoxColumn bClose;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SocialSecurity;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstN;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Addr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BID;
    }
}

